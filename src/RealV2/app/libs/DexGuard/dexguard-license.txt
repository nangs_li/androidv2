# DexGuard license file
licensee              = Real Corporation Limited
contact               = Thomas Ma
contactEmail          = taisangtech@gmail.com
product               = DexGuard
edition               = Enterprise
version               = 7.*
package               = co.real.productionreal2
expiryDate            = 2017-03-24
maintenanceExpiryDate = 2017-03-24
key                   = 07b841acb483a69e3c6031908a99ce5fc5997f00fb808982aefdc1ae9dac2d617587ffa5d46e5fbd77f816af7cd51fd7b6440f9b6236b11cbec9d4a4879ec8bd
