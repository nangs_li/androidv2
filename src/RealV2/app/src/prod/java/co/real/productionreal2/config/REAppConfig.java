package co.real.productionreal2.config;

/**
 * Created by kelvinsun on 29/1/16.
 */
public class REAppConfig {

    //PROD
    public static final Boolean IS_DEV = false;

    public static String getRealApiBaseUrl(){
        return "https://api.real.co/v2/";
    }

    public static String getQbAppId(){
        return "3";
    }

    public static String getQbAuthKey(){
        return "5seV5xSrdH2JnOw";
    }

    public static String getQbAuthSecret(){
        return "8wQXPKu93GNz6JO";
    }

    public static String getQbContentBucket(){
        return "qb-forreal-s3";
    }

    public static String getQbApiEndpoint(){
        return "apiforreal.quickblox.com";
    }

    public static String getQbChatEndpoint(){
        return "chatforreal.quickblox.com";
    }

    public static String getQbTurnEndpoint(){
        return "turnserver.quickblox.com";
    }

    public static String getBranchKey(){
        return "key_live_knoLWISWfYY2N4UYZdK7bhkbECfWrXww";
    }

    public static String getMixpanelToken(){
        return "103f8fc687e6610385b933a3cec18019";
    }

    public static String getGoogleApiKey (){
        return "AIzaSyAUvXzefUL7vdyqiYpDOkJ2TJzbOm-2R_I";
    }

    public static String getGoogleApiUrl (){
        return "https://www.googleapis.com";
    }

    public static String getGoogleMapApiUrl (){
        return "https://maps.googleapis.com/";
    }

    public static String getCryptKey() {
        return "03ac674216f3e15c761ee1a5e255f067";
    }

    public static String getCryptIv() {
        return "mS++sv+0xEL0YwM=";
    }

    public static String getCryptCx() {
        return "AES/CBC/PKCS5Padding";
    }

}
