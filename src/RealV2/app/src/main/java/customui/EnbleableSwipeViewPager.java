package customui;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by edwinchan on 15/1/16.
 */
public class EnbleableSwipeViewPager extends ViewPager {

    boolean isSwipeable = false;
    boolean tempEnable = false;

    public EnbleableSwipeViewPager(Context context) {
        super(context);
        isSwipeable = false;
        tempEnable = false;

    }

    public EnbleableSwipeViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        isSwipeable = false;
        tempEnable = false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
//        Log.e("","edwin EnbleableSwipeViewPager onInterceptTouchEvent isSwipeable "+isSwipeable+" "+event.getAction());
        if(isSwipeable){
            return super.onInterceptTouchEvent(event);
        }else {
            return false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
//        Log.e("", "edwin EnbleableSwipeViewPager onTouchEvent isSwipeable " + isSwipeable + " " + event.getAction());
//        if(tempEnable && (event.getAction() == MotionEvent.ACTION_CANCEL || event.getAction() == MotionEvent.ACTION_UP)){
//            isSwipeable = false;
//            tempEnable = false;
//        }

        if(isSwipeable){
            return super.onTouchEvent(event);
        }else {
            return false;
        }
    }

    public boolean runOnInterceptTouchEvent(MotionEvent event){
        boolean b = super.onInterceptTouchEvent(event);
//        Log.i("", "edwin EnbleableSwipeViewPager onInterceptTouchEvent b " + b);
        return b;
    }

    public void setSwipeable(boolean enable){
        isSwipeable = enable;
        tempEnable = enable;
//        Log.w("", "edwin EnbleableSwipeViewPager setSwipeable isSwipeable " + isSwipeable);
    }

    public boolean isSwipeable(){
        return isSwipeable;
    }

    public void setDisable(){
//        Log.i("", "edwin EnbleableSwipeViewPager setDisable ");
        isSwipeable = false;
        tempEnable = false;
    }

}