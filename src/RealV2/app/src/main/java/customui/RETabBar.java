package customui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.view.View;

import java.util.List;
import java.util.Random;

/**
 * Created by kelvinsun on 4/8/16.
 */
public class RETabBar extends TabLayout {

    private Context context;
    private List<RETabView> tabViews;
    private RETabBarListener reTabBarListener;

    public static enum TAB_SECTION{SEARCH,NEWS,CHATS,ME,SETTINGS};
    public TAB_SECTION tabSection;

    public interface RETabBarListener{
        void onTabSelected(TabLayout.Tab tab);
    }

    public RETabBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public RETabBar(Context context) {
        super(context);
        this.context = context;

    }

    public void setTabViews(List<RETabView> tabViews, final RETabBarListener reTabBarListener) {
        this.tabViews = tabViews;
        this.reTabBarListener=reTabBarListener;
        for (RETabView tabView : tabViews) {
            addTab(newTab().setCustomView(tabView));
        }
        //add tab listener
        setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                reTabBarListener.onTabSelected(tab);

                //TODO: TEST ONLY for bubble
                Random r = new Random();
                int i1 = r.nextInt(5);
                int i2 = r.nextInt(3);
                updateBubbleCount(i1, i2);
                updateBubbleCount(tab.getPosition(), 0); //reset bubble
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void selectAtIndex(int tabIndex){
    }

    public void show() {
        show(true);
    }

    public void hide() {
       show(false);
    }

    public void show(boolean isShow) {
        setVisibility(isShow?View.VISIBLE:View.GONE);
//        animate()
//                .translationY(0)
//                .alpha(0.0f)
//                .setListener(new AnimatorListenerAdapter() {
//                    @Override
//                    public void onAnimationEnd(Animator animation) {
//                        super.onAnimationEnd(animation);
//                        setVisibility(View.GONE);
//                    }
//                });
    }


    public void updateBubbleCount(int atIndex, int count) {
        ((RETabView) getTabAt(atIndex).getCustomView()).setBubble(count);
    }


}
