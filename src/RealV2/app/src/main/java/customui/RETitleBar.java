package customui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import co.real.productionreal2.R;
import co.real.productionreal2.fragment.BaseFragment;
import co.real.productionreal2.fragment.RESearchMainFragment;
import customui.viewpagerindicator.CirclePageIndicator;

/**
 * Created by kelvinsun on 5/8/16.
 */
public class RETitleBar extends LinearLayout {

    TextView tvTitle;
    SearchView svSearch;
    LinearLayout llThreeDots;
    CirclePageIndicator pageIndicator;
    ViewPager vpThreeDots;
    LinearLayout llTitleSection;

    ThreeDotsPagerAdapter threeDotsPagerAdapter;

    public RETitleBar(Context context) {
        super(context);
    }

    public RETitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        llTitleSection = (LinearLayout) findViewById(R.id.ll_title_section);
        tvTitle = (TextView) findViewById(R.id.tv_title_name);
        svSearch = (SearchView) findViewById(R.id.sv_address);
//        llThreeDots = (LinearLayout) findViewById(R.id.ll_three_dots);
        pageIndicator= (CirclePageIndicator) findViewById(R.id.pi_three_dots);
        vpThreeDots=(ViewPager) findViewById(R.id.vp_three_dots);

    }

    public void setTitle(String title) {
        svSearch.setVisibility(View.GONE);
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText(title);
    }

    public void showSearchView(boolean enableSearch) {
        tvTitle.setVisibility(enableSearch ? View.GONE : View.VISIBLE);
        svSearch.setVisibility(enableSearch ? View.VISIBLE : View.GONE);
    }

    public void show() {
        show(true);
    }

    public void hide() {
        show(false);
    }

    public void show(boolean isShow) {
        llTitleSection.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    public void showThreeDots() {
        showThreeDots(true);
    }

    public void hideThreeDots() {
        showThreeDots(false);
    }

    public void showThreeDots(boolean isShow) {
        pageIndicator.setVisibility(isShow ? View.VISIBLE : View.GONE);
        //testing
        setPagerIndicator(1);
    }

    public void setPagerIndicator(int atIndex){
        threeDotsPagerAdapter = new ThreeDotsPagerAdapter();
        vpThreeDots.setAdapter(threeDotsPagerAdapter);
        pageIndicator.setViewPager(vpThreeDots);
        pageIndicator.setCurrentItem(atIndex);
    }

    public SearchView getSvSearch() {
        return svSearch;
    }


    //Attaching the fragments to the tabPagerAdapter
    public class ThreeDotsPagerAdapter extends PagerAdapter {

        @Override
        public Object instantiateItem(final ViewGroup container, int position) {
            Button button = new Button(container.getContext());
            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            button.setLayoutParams(params);
            button.setText(String.valueOf(position));

            LinearLayout layout = new LinearLayout(container.getContext());
            layout.setOrientation(LinearLayout.VERTICAL);
            //to container add layout instead of button
            container.addView(layout);
            //return layout instead of button
            return layout;
        }

        @Override
        public int getCount() {
            // get item count - equal to number of tabs
            return 3;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return false;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            //cast to LinearLayout
            container.removeView((LinearLayout)object);
        }
    }
}
