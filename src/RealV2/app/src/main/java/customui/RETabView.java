package customui;

import android.content.Context;
import android.graphics.drawable.StateListDrawable;
import android.util.StateSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import co.real.productionreal2.R;

/**
 * Created by kelvinsun on 4/8/16.
 */
public class RETabView extends LinearLayout {
    View tabView;
    TextView tvTitle;
    ImageView ivIcon;
    TextView tvBubble;

    public RETabView(Context context,int normalDrawableState,int selectedDrawableState) {
        super(context);
        init(context,normalDrawableState,selectedDrawableState);
    }

    private void init(Context context,int normalDrawableState,int selectedDrawableState) {
        tabView = inflate(context, R.layout.view_tab_view, this);
        tvTitle = (TextView) tabView.findViewById(R.id.tv_tab_title);
        ivIcon = (ImageView) tabView.findViewById(R.id.iv_tab_icon);
        tvBubble = (TextView) tabView.findViewById(R.id.tv_tab_bubble);

        StateListDrawable drawable=new StateListDrawable();
        drawable.addState(new int[] { android.R.attr.state_pressed },  getResources().getDrawable(selectedDrawableState));
        drawable.addState(new int[] { android.R.attr.state_selected },  getResources().getDrawable(selectedDrawableState));
        drawable.addState(StateSet.WILD_CARD, getResources().getDrawable(normalDrawableState));
        ivIcon.setImageDrawable(drawable);
    }

    public void setBubble(int payload){

        tvBubble.setVisibility(payload>0?View.VISIBLE:View.GONE);
        tvBubble.setText(String.valueOf(payload));
    }

}
