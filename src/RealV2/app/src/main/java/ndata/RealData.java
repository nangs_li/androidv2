package ndata;

/**
 * Created by edwinchan on 15/3/16.
 */
public class RealData {

    public static native String getKey();

    static {
        System.loadLibrary("real-jni");
    }
}
