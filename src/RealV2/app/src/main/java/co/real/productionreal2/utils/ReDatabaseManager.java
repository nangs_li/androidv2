package co.real.productionreal2.utils;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by edwinchan on 27/5/16.
 */
public class ReDatabaseManager {

    private static Realm realm;

    /**
     *  Configure Realm for the application
     */
    public static void init(Context context){
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(context).build();
//        Realm.deleteRealm(realmConfiguration); // Clean slate
        Realm.setDefaultConfiguration(realmConfiguration); // Make this Realm the default
////        realm = Realm.getInstance(realmConfiguration);
    }

//    public static Realm getInstance(Context context) {
//        if (realm == null) {
//            init(context);
//        }
//        return realm;
//    }

    /**
     *  Configure Realm for the application
     */
    public void close(){
        realm.close();
    }

}
