package co.real.productionreal2.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import co.real.productionreal2.R;
import co.real.productionreal2.model.REAgentListing;
import co.real.productionreal2.model.REAgentProfile;

/**
 * Created by kelvinsun on 14/7/16.
 */
public class ListingSearchAdapter extends RecyclerView.Adapter<ListingSearchAdapter.AddressViewHolder> {

    //    List<REAgentListing> agentProfileList =new ArrayList<>();
    AdapterView.OnItemClickListener onItemClickListener;
    ListingSearchAdapterListener mListener;
    Context context;

    public ListingSearchAdapter(Context context, AdapterView.OnItemClickListener onItemClickListener, ListingSearchAdapterListener mListener) {
        this.context = context;
        this.onItemClickListener = onItemClickListener;
        this.mListener = mListener;
    }

    public ListingSearchAdapter(Context context, ListingSearchAdapterListener mListener) {
        this.context = context;
        this.mListener = mListener;
    }


    public interface ListingSearchAdapterListener {
        List<REAgentListing> getAgentListingList();
        List<REAgentProfile> getAgentProfileList();
    }

    @Override
    public AddressViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listing, parent, false);
        return new AddressViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AddressViewHolder holder, int position) {

        Log.d("onBindViewHolder", "onBindViewHolder: 1st: "+mListener.getAgentProfileList().size() +"  "+mListener.getAgentListingList().size() );
        REAgentListing agentListing = mListener.getAgentListingList().get(position);
        REAgentProfile agentProfile = mListener.getAgentProfileList().get(position);


        String address=agentListing.getFullAddressWithLocale();

        holder.addressName.setText("MemberId: " + agentProfile.realMemberID +
                "\nLicenseNo: \"" + agentProfile.licenseNumber + "\"" +
                "\nListingID: " + agentListing.agentListingID +
                " \nAddress: " + address);


        Picasso.with(context).load(agentListing.getListingCoverPhoto()).into(holder.ivCover);
        Picasso.with(context).load(agentProfile.photoURL).into(holder.ivPic);

//        Log.d("onBindViewHolder", "onBindViewHolder: 1st: "+address.getPrimaryText(null));
//        Log.d("onBindViewHolder", "onBindViewHolder: 2nd: "+address.getSecondaryText(null));

    }

    @Override
    public int getItemCount() {
        if (mListener.getAgentProfileList().size() == 0 || mListener.getAgentListingList().size() == 0) {
            return 0;
        } else {
            return mListener.getAgentProfileList().size();
        }
    }

    public class AddressViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected ImageView ivCover;
        protected TextView addressName;
        protected ImageView ivPic;


        public AddressViewHolder(View v) {
            super(v);
            ivCover= (ImageView)v.findViewById(R.id.iv_cover_photo);
            addressName = (TextView) v.findViewById(R.id.tv_listing_name);
            ivPic = (ImageView) v.findViewById(R.id.iv_listing_pic);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
//            onItemClickListener.onItemClick(null, v, getAdapterPosition(), v.getId());
        }
    }
}
