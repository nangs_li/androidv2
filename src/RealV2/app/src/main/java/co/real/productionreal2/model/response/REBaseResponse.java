package co.real.productionreal2.model.response;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import co.real.productionreal2.model.REAddressSearchResult;
import co.real.productionreal2.model.REAgentListing;
import co.real.productionreal2.model.REAgentProfile;

/**
 * Created by kelvinsun on 15/7/16.
 */
public class REBaseResponse {
    @SerializedName("ErrorCode")
    public int errorCode;

    @SerializedName("ApiName")
    public String apiName;

    @SerializedName("UniqueKey")
    public String uniqueKey;

    @Nullable
    @SerializedName("FullRecordSet")
    public List<REAddressSearchResult> fullRecordSet;

    @Nullable
    @SerializedName("AgentProfiles")
    public List<REAgentProfile> agentProfiles;

    @Nullable
    @SerializedName("AgentListings")
    public List<REAgentListing> agentListings;

    public REBaseResponse(int errorCode, String apiName, String uniqueKey) {
        this.errorCode = errorCode;
        this.apiName = apiName;
        this.uniqueKey = uniqueKey;
    }

    public REBaseResponse() {
    }

//    public REBaseResponse(int errorCode, String apiName, String uniqueKey, List<Integer> fullRecordSet, List<REAgentProfile> agentProfiles, List<REAgentListing> agentListings) {
//        this.errorCode = errorCode;
//        this.apiName = apiName;
//        this.uniqueKey = uniqueKey;
//        this.fullRecordSet = fullRecordSet;
//        this.agentProfiles = agentProfiles;
//        this.agentListings = agentListings;
//    }


    @Override
    public String toString() {
        return "REBaseResponse{" +
                "errorCode=" + errorCode +
                ", apiName='" + apiName + '\'' +
                ", uniqueKey='" + uniqueKey + '\'' +
                ", fullRecordSet=" + fullRecordSet +
                ", agentProfiles='" + agentProfiles + '\'' +
                ", agentListings='" + agentListings + '\'' +
                '}';
    }
}
