package co.real.productionreal2.model.db;

import io.realm.RealmObject;

/**
 * Created by kelvinsun on 13/7/16.
 */
public class RealmInteger extends RealmObject {
    private Integer val;

    public RealmInteger(Integer val) {
        this.val = val;
    }

    public RealmInteger() {
    }

    public Integer getValue() {
        return val;
    }

    public void setValue(Integer value) {
        this.val = value;
    }
}
