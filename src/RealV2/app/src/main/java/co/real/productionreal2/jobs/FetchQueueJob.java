package co.real.productionreal2.jobs;

import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;

import java.util.concurrent.atomic.AtomicInteger;

import co.real.productionreal2.model.REQueueJob;
import co.real.productionreal2.model.REQueueModel;

/**
 * Created by kelvinsun on 26/5/16.
 */
public class FetchQueueJob extends Job {
        private static final AtomicInteger jobCounter = new AtomicInteger(0);

        private final int id;
        public FetchQueueJob() {
            super(new Params(Priority.LOW).requireNetwork().persist());
            id = jobCounter.incrementAndGet();
        }

        @Override
        public void onAdded() {

        }

        @Override
        public void onRun() throws Throwable {
            if(id != jobCounter.get()) {
                //looks like other fetch jobs has been added after me. no reason to keep fetching
                //many times, cancel me, let the other one fetch tweets.
                return;
            }
            REQueueModel tweetModel = REQueueModel.getInstance();
            REQueueJob lastQueueJob = tweetModel.getLastQueueJob();
//            List<Status> statusList = TwitterController.getInstance().loadTweets(lastTweet == null ? null : lastTweet.getServerId());
//            if(statusList.size() > 0) {
//                List<REQueueJob> tweets = new ArrayList<REQueueJob>(statusList.size());
//                for(Status status : statusList) {
//                    Tweet tweet = new Tweet(status);
//                    tweets.add(tweet);
//                }
//                tweetModel.insertOrReplaceAll(tweets);
//                EventBus.getDefault().post(new FetchedNewTweetsEvent());
//            }
        }

        @Override
        protected void onCancel() {
            //TODO show error notification
        }

        @Override
        protected boolean shouldReRunOnThrowable(Throwable throwable) {
//            if(throwable instanceof TwitterException) {
//                //if it is a 4xx error, stop
//                TwitterException twitterException = (TwitterException) throwable;
//                return twitterException.getErrorCode() < 400 || twitterException.getErrorCode() > 499;
//            }
            return true;
        }
    }
