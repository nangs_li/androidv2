package co.real.productionreal2.model.db;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by edwinchan on 8/7/16.
 */
public class REDBAPIQueue extends RealmObject {

    @PrimaryKey
    public long queueId;
    public int httpMethod;
    public RealmList<RealmInteger> dependingQueues;
    public RealmList<RealmInteger> pendingQueues;
    public int retryCount;
    public String response;
    public String url;
    public String parameter;
    public int status;

    @Override
    public String toString() {
        return "REDBAPIQueue{" +
                "queueId=" + queueId +
                ", httpMethod=" + httpMethod +
                ", dependingQueues=" + dependingQueues +
                ", pendingQueues=" + pendingQueues +
                ", retryCount=" + retryCount +
                ", response='" + response + '\'' +
                ", url='" + url + '\'' +
                ", parameter='" + parameter + '\'' +
                ", status=" + status +
                '}';
    }

//    public REDBObject toREDBObject(){return null;}
}