package co.real.productionreal2.model.request;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.BuildConfig;
import co.real.productionreal2.REApplication;
import co.real.productionreal2.model.response.BaseResponse;
import co.real.productionreal2.network.REApiCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kelvinsun on 20/5/16.
 */
public class REReqAppVersion {

    @SerializedName("MyVersion")
    public String myVersion;

    @SerializedName("DeviceType")
    public int deviceType;

    public REReqAppVersion(int deviceType, String myVersion) {
        this.deviceType = deviceType;
        this.myVersion = myVersion;
    }

    /**
     * default values
     */
    public REReqAppVersion() {
        super();
        this.myVersion = BuildConfig.VERSION_NAME;
        this.deviceType = 2;
    }

    public void callCheckVersion(final Context context, final REApiCallback callback) {

        Call<BaseResponse> makeCall = REApplication.getReApiService().checkVersion(new BaseRequest(this));
        makeCall.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseResponse baseResponse = response.body();
                // at this point the JSON body has been successfully parsed
                if (baseResponse != null) {
                    BaseResponse.RspBase rspBase = new Gson().fromJson(baseResponse.jsonContent, BaseResponse.RspBase.class);
                    int statusCode= rspBase.errorCode;

//                    if (response.isSuccessful() && REApiClient.checkResponseSuccess(statusCode)) {
//                            callback.success(baseResponse);
//                    } else {
//                        if (!REApiClient.checkIsUserOverride(context,statusCode)) {
//                            REApiError reApiError = new REApiError(statusCode, rspBase.errorMsg);
//                            callback.failure(reApiError);
//                        }else{
//                            //already handled by REApiClient
//                        }
//                    }

                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Log.d("onResponse", "onFailure: " + call);
            }
        });
    }
}
