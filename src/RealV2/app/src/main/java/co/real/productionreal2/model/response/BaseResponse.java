package co.real.productionreal2.model.response;

/**
 * Created by kelvinsun on 24/5/16.
 */

import com.google.gson.annotations.SerializedName;

/**
 * Created by kelvinsun on 20/5/16.
 */
public class BaseResponse {
    @SerializedName("d")
    public String jsonContent;

    @Override
    public String toString() {
        return "BaseResponse{" +
                "jsonContent='" + jsonContent + '\'' +
                '}';
    }

    public class RspBase {

        @SerializedName("errorCode")
        public int errorCode;

        @SerializedName("errorMsg")
        public String errorMsg;

        @SerializedName("apiName")
        public String apiName;

        @Override
        public String toString() {
            return "RspBase{" +
                    "errorCode=" + errorCode +
                    ", errorMsg='" + errorMsg + '\'' +
                    ", apiName='" + apiName + '\'' +
                    '}';
        }
    }
}

