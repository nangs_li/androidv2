package co.real.productionreal2.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by kelvinsun on 8/8/16.
 */
public class RELanguage extends RealmObject {
    @SerializedName("Language")
    public String language;
    @SerializedName("LangIndex")
    public int langIndex;
}
