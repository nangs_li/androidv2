package co.real.productionreal2.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by kelvinsun on 8/8/16.
 */
public class REPhoto  extends RealmObject {
        @SerializedName("PhotoID")
        public int PhotoID;
        @SerializedName("Position")
        public int Position;
        @SerializedName("URL")
        public String URL;
        @SerializedName("Width")
        public int width;
        @SerializedName("Height")
        public int height;
    }
