package co.real.productionreal2.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kelvinsun on 20/5/16.
 */
public class RspAppVersion extends BaseResponse {
    @SerializedName("Ver")
    public Ver ver;

    @SerializedName("URL")
    public String url;

    public class Ver {
        @SerializedName("androidVer")
        public String androidVer;
        @SerializedName("ForceUpdate")
        public int forceUpdate;
    }

    @Override
    public String toString() {
        return "RspAppVersion{" +
                "ver=" + ver +
                ", url='" + url + '\'' +
                '}';
    }

}
