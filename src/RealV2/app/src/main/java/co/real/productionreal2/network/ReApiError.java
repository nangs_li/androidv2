package co.real.productionreal2.network;

/**
 * Created by kelvinsun on 20/5/16.
 */
public class REApiError {
    private int statusCode;
    private String message;

    public REApiError() {
    }

    public REApiError(int statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
    }

    public int status() {
        return statusCode;
    }

    public String message() {
        return message;
    }

    @Override
    public String toString() {
        return "REApiError{" +
                "statusCode=" + statusCode +
                ", message='" + message + '\'' +
                '}';
    }
}
