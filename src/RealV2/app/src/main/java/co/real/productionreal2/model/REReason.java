package co.real.productionreal2.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by kelvinsun on 8/8/16.
 */
public class REReason extends RealmObject {
        @SerializedName("LanguageIndex")
        public int languageIndex;
        @SerializedName("SloganIndex")
        public int SloganIndex;
        @SerializedName("Reason1")
        public String reason1;
        @SerializedName("Reason2")
        public String reason2;
        @SerializedName("Reason3")
        public String reason3;
        @SerializedName("Reason1URL")
        public String reason1Url;
        @SerializedName("Reason2URL")
        public String reason2Url;
        @SerializedName("Reason3URL")
        public String reason3Url;
    }