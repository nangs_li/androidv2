package co.real.productionreal2.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.real.productionreal2.R;
import customui.EnbleableSwipeViewPager;
import customui.RETabBar;
import customui.RETabView;
import customui.RETitleBar;

/**
 * Created by kelvinsun on 5/8/16.
 */
public class REMainFragment extends BaseFragment implements RETabBar.RETabBarListener {

    @BindView(R.id.title_bar)
    RETitleBar titleBar;
    @BindView(R.id.tab_bar)
    RETabBar tabBar;
    @BindView(R.id.vp_main)
    EnbleableSwipeViewPager viewPager;

    private TabsPagerAdapter sectionsPagerAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initTabView();
        initTitleBar();
        initTabLayout();
        initView();

    }

    private void initTabLayout() {
        sectionsPagerAdapter = new TabsPagerAdapter(getActivity().getSupportFragmentManager());
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(sectionsPagerAdapter.getCount());
        viewPager.setSwipeable(false);

        viewPager.setAdapter(sectionsPagerAdapter);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                // on changing the page
                titleBar.setTitle("Tab " + position);
                // make respected tab selected
                tabBar.selectAtIndex(position);
                //show search view
                titleBar.showSearchView(position==0);
                titleBar.showThreeDots(position!=4);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initView() {
       //TODO
    }

    private void initTabView() {
        List<RETabView> tabViews = new ArrayList<>();
        tabViews.add(new RETabView(getActivity(), R.drawable.ico_tab_search_on, R.drawable.ico_tab_search_off));
        tabViews.add(new RETabView(getActivity(), R.drawable.ico_tab_newsfeed_on, R.drawable.ico_tab_newsfeed_off));
        tabViews.add(new RETabView(getActivity(), R.drawable.ico_tab_chats_on, R.drawable.ico_tab_chats_off));
        tabViews.add(new RETabView(getActivity(), R.drawable.ico_tab_me_on, R.drawable.ico_tab_me_off));
        tabViews.add(new RETabView(getActivity(), R.drawable.ico_tab_setting_on, R.drawable.ico_tab_setting_off));
        tabBar.setTabViews(tabViews, this);
    }

    private void initTitleBar() {
    }


    public void pushFragment(BaseFragment baseFragment) {
        android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fl_main_fragment_container, baseFragment);
        ft.addToBackStack(baseFragment.getClass().getName());
        ft.commit();
    }

    public void popFragment() {
        // TODO
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition(),false);
    }


    //Attaching the fragments to the tabPagerAdapter
    public class TabsPagerAdapter extends FragmentPagerAdapter {

        public TabsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public BaseFragment getItem(int index) {
            Log.d("TabsPagerAdapter","TabsPagerAdapter: "+index);
            BaseFragment baseFragment=RESearchMainFragment.newInstance("Address Search");
            switch (index) {
                case 0:
                    return baseFragment;
                case 1:
                    //TODO
                    return RESearchMainFragment.newInstance("Tab "+index);
                case 2:
                    //TODO
                    return RESearchMainFragment.newInstance("Tab "+index);
                case 3:
                    //TODO
                    return RESearchMainFragment.newInstance("Tab "+index);
                case 4:
                    //TODO
                    return RESearchMainFragment.newInstance("Tab "+index);
            }

            return null;
        }
        @Override
        public int getCount() {
            // get item count - equal to number of tabs
            return RETabBar.TAB_SECTION.values().length;
        }
    }
}
