package co.real.productionreal2.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kelvinsun on 15/7/16.
 */
public class REBaseRequest  {
    @SerializedName("ApiName")
    public String apiName;

    @SerializedName("LanguageIndex")
    public int languageIndex;

    @SerializedName("UniqueKey")
    public String uniqueKey;

    public REBaseRequest(String apiName, int languageIndex, String uniqueKey) {
        this.apiName = apiName;
        this.languageIndex = languageIndex;
        this.uniqueKey = uniqueKey;
    }
}
