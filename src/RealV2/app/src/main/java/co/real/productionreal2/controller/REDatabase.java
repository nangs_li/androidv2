package co.real.productionreal2.controller;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by edwinchan on 27/5/16.
 */
public class REDatabase {

    final static int kSchemaVersion = 1;
    final static String kEncryptionKey = "C85DCB777962817BCADBFC322D7F69F20F8B5CDB5FFDE30CB611E0F4D140F72A";

    private static Realm realm;

    /**
     * Configure Realm for the application
     */
    public static void init(Context context) {
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(context)
                .encryptionKey(kEncryptionKey.getBytes())
                .schemaVersion(kSchemaVersion)
                .build();
        Realm.deleteRealm(realmConfiguration); // Clean slate
        Realm.setDefaultConfiguration(realmConfiguration); // Make this Realm the default
////        realm = Realm.getInstance(realmConfiguration);
    }

    public static Realm getInstance(Context context) {
        if (realm == null) {
            init(context);
        }
        return realm;
    }

    /**
     * Configure Realm for the application
     */
    public void close() {
        realm.close();
    }


    /**
     * pragma mark - General
     */
    public void deleteObject(final RealmObject object){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                object.deleteFromRealm(); // indirectly delete object
            }
        });
    }
    public void deleteObjects(final RealmResults objects){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                objects.deleteAllFromRealm();
            }
        });
    }
    public void deleteAllDBData(final RealmResults objects){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
//                Realm.deleteRealm();
            }
        });
    }


    /**
     * pragma mark - APIQueue
     */



}
