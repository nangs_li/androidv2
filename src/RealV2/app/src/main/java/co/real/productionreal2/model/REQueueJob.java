package co.real.productionreal2.model;

/**
 * Created by kelvinsun on 26/5/16.
 */
public class REQueueJob {
    protected Long queueId;
    protected Object object;

    protected int type;
    protected int[] dependencyIds;
    protected int[] pendingIds;
    protected int retryCount;
    protected String response;
    protected String url;
    protected String parameter;
    protected String status;
    protected String key;

    public REQueueJob(Long queueId, int type, int[] dependencyIds, int[] pendingIds, int retryCount, String response, String url, String parameter, String status, String key) {
        this.queueId = queueId;
        this.type = type;
        this.dependencyIds = dependencyIds;
        this.pendingIds = pendingIds;
        this.retryCount = retryCount;
        this.response = response;
        this.url = url;
        this.parameter = parameter;
        this.status = status;
        this.key = key;
    }


    public REQueueJob() {
    }

    public REQueueJob(Long queueId) {
        this.queueId = queueId;
    }

    // KEEP METHODS - put your custom methods here
    public REQueueJob(String status) {
        this.status = status;
    }

    public Long getQueueId() {
        return queueId;
    }

    public void setQueueId(Long queueId) {
        this.queueId = queueId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int[] getDependencyIds() {
        return dependencyIds;
    }

    public void setDependencyIds(int[] dependencyIds) {
        this.dependencyIds = dependencyIds;
    }

    public int[] getPendingIds() {
        return pendingIds;
    }

    public void setPendingIds(int[] pendingIds) {
        this.pendingIds = pendingIds;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
