package co.real.productionreal2.model.db;

import io.realm.RealmObject;

/**
 * Created by edwinchan on 27/5/16.
 */
public class TestingDBUser extends RealmObject {

    public String name;
    public int Type;

    public int Field1;
    public int Field2;
    public int Field3;
    public int Field4;
    public int Field5;
    public int Field6;
    public int Field7;
    public int Field8;
    public int Field9;
    public int Field10;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }
}
