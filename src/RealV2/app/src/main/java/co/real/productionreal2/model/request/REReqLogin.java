package co.real.productionreal2.model.request;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.REApplication;
import co.real.productionreal2.model.response.BaseResponse;
import co.real.productionreal2.network.REApiCallback;
import co.real.productionreal2.network.REApiClient;
import co.real.productionreal2.network.REApiError;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kelvinsun on 26/5/16.
 */
public class REReqLogin {
    @SerializedName("deviceToken")
    public String deviceToken;

    @SerializedName("deviceType")
    public int deviceType;

    @SerializedName("userName")
    public String userName;

    @SerializedName("socialNetworkType")
    public int socialNetworkType;

    @SerializedName("email")
    public String email;

    @SerializedName("userID")
    public String userID;

    @SerializedName("photoURL")
    public String photoURL;

    @SerializedName("lang")
    public String lang;

    @SerializedName("accessToken")
    public String accessToken;

    public REReqLogin(String deviceToken, int deviceType, String userName, int socialNetworkType, String email, String userID, String photoURL, String lang, String accessToken) {
        this.deviceToken = deviceToken;
        this.deviceType = deviceType;
        this.userName = userName;
        this.socialNetworkType = socialNetworkType;
        this.email = email;
        this.userID = userID;
        this.photoURL = photoURL;
        this.lang = lang;
        this.accessToken = accessToken;
    }

    public void callLogin(REApiClient reApiClient, final REApiCallback callback) {

        Call<BaseResponse> makeCall = REApplication.getReApiService().login(new BaseRequest(this));
//        makeCall.enqueue(reApiClient.getJSONResponseCallBack(callback));
    }


    @Override
    public String toString() {
        return "ReqLogin{" +
                "deviceToken='" + deviceToken + '\'' +
                ", deviceType=" + deviceType +
                ", userName='" + userName + '\'' +
                ", socialNetworkType=" + socialNetworkType +
                ", email='" + email + '\'' +
                ", userID='" + userID + '\'' +
                ", photoURL='" + photoURL + '\'' +
                ", lang='" + lang + '\'' +
                ", accessToken='" + accessToken + '\'' +
                '}';
    }

    public void callLogin(final Context context, final REApiCallback callback) {

        Call<BaseResponse> makeCall = REApplication.getReApiService().login(new BaseRequest(this));
        makeCall.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseResponse baseResponse = response.body();
                // at this point the JSON body has been successfully parsed
                if (baseResponse != null) {
                    BaseResponse.RspBase rspBase = new Gson().fromJson(baseResponse.jsonContent, BaseResponse.RspBase.class);
                    int statusCode= rspBase.errorCode;
                    if (response.isSuccessful() && REApiClient.checkResponseSuccess(statusCode)) {
//                        callback.success(baseResponse);
                    } else {
                        if (!REApiClient.checkIsUserOverride(statusCode)) {
                            REApiError REApiError = new REApiError(statusCode, rspBase.errorMsg);
                            callback.failure(REApiError);
                        }else{
                            //already handled by REApiClient
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Log.d("onResponse", "onFailure: " + call);
            }
        });

    }
}
