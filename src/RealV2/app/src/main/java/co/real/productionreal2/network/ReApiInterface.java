package co.real.productionreal2.network;

import co.real.productionreal2.model.request.BaseRequest;
import co.real.productionreal2.model.request.REReqAddressSearch;
import co.real.productionreal2.model.request.REReqAgentListing;
import co.real.productionreal2.model.request.REReqAgentProfile;
import co.real.productionreal2.model.response.BaseResponse;
import co.real.productionreal2.model.response.REBaseResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Url;

/**
 * Created by kelvinsun on 20/5/16.
 */

/* Retrofit 2.0 */

public interface REApiInterface {

    // Request method and URL specified in the annotation
    // Callback for the parsed response is the last parameter


    //SAMPLE API
    @GET("users/{username}")
    Call<BaseResponse> getUser(@Path("username") String username);

    //GENERAL API
    @POST
    Call<BaseResponse> getGeneralResponse(@Url String anEmptyString,@Body BaseRequest baseRequest);

    @POST("Operation.asmx/Version")
    Call<BaseResponse> checkVersion(@Body BaseRequest baseRequest);

    @POST("V2_Admin.asmx/Login")
    Call<BaseResponse> login(@Body BaseRequest baseRequest);

    @POST("Listing")
    Call<REBaseResponse> addressSearch(@Body REReqAddressSearch reqAddressSearch);

    @POST("Listing")
    Call<REBaseResponse> agentProfileGet(@Body REReqAgentProfile reqAgentProfile);

    @POST("Listing")
    Call<REBaseResponse> agentListingGet(@Body REReqAgentListing reqAgentListing);
}
