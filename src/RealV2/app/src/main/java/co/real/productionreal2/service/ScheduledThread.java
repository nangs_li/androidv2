package co.real.productionreal2.service;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import co.real.productionreal2.model.request.REReqLogin;

/**
 * To perform a background schedule job regularly
 * Created by kelvinsun on 19/5/16.
 */
public class ScheduledThread {
    private static ScheduledExecutorService scheduleTaskExecutor;
    private static final int INTERVAL = 10; //in second
    private static final int POOL_SIZE = 5; // max number of threads

    /**
     * start delay schedule job with UI change
     *
     * @param context
     */
    public static void startJob(final Context context) {
        clearJob();
        // This schedule a task to run every N minutes:
        scheduleTaskExecutor = Executors.newScheduledThreadPool(POOL_SIZE);
        scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
            public void run() {
                // Do something
                Date currentDate = new Date(System.currentTimeMillis());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
                final String currentDateandTime = sdf.format(currentDate);
                //api sample
                REReqLogin reqLogin = new REReqLogin("123", 2, "kelivnsun", 1, "kelvin.sun@hohojo.com", "123888", "http://hk.yahoo.com", "en", "1241234");
//                reqLogin.callLogin(REApplication.get().getReApiClient(), new REApiCallback() {
//                    @Override
//                    public void success(BaseResponse baseResponse) {
//                        RspLogin rspLogin = new Gson().fromJson(baseResponse.jsonContent, RspLogin.class);
//                        if (rspLogin != null) {
//                            EventBus.getDefault().post(new MessageEvent("SUCCESS "+rspLogin.toString()));
//                        }
//                    }
//
//                    @Override
//                    public void failure(REApiError reApiError) {
//                        if (reApiError != null) {
//                            EventBus.getDefault().post(new MessageEvent("FAIL "+reApiError.toString()));
//                        } else {
//                            EventBus.getDefault().post(new MessageEvent("Network not available"));
//                        }
//                    }
//                });


                // If you need update UI, simply do this:
//                ((Activity) context).runOnUiThread(new Runnable() {
//                    public void run() {
//                        // update your UI component here.
//                        Date currentDate = new Date(System.currentTimeMillis());
//                        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
//                        String currentDateandTime = sdf.format(currentDate);
//                        AppUtil.showToast(context, currentDateandTime);
//                    }
//                });
            }
        }, INTERVAL, INTERVAL, TimeUnit.SECONDS);
    }

    /**
     * start delay schedule job without UI change
     */
    public static void startJob() {
        clearJob();
        // This schedule a task to run every N minutes:
        scheduleTaskExecutor = Executors.newScheduledThreadPool(POOL_SIZE);
        scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
            public void run() {
                // Do something
            }
        }, INTERVAL, INTERVAL, TimeUnit.SECONDS);

    }


    public static class ScheduledPrinter implements Callable<String> {
        public String call() throws Exception {
            return "somethhing";
        }
    }


    /**
     * start schedule job with callback
     */
    public static void startJobWithCallback() {
        clearJob();
        // This schedule a task to run every N minutes:
        scheduleTaskExecutor = Executors.newScheduledThreadPool(POOL_SIZE);
        scheduleTaskExecutor.schedule(new ScheduledPrinter(), INTERVAL, TimeUnit.SECONDS);

    }


    /**
     * dismiss existing schedule job
     */
    public static void clearJob() {
        if (scheduleTaskExecutor != null) {
            scheduleTaskExecutor.shutdown();
        }
    }

    public static boolean isRunning() {
        return scheduleTaskExecutor != null;
    }

}
