package co.real.productionreal2.network;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import co.real.productionreal2.config.REAppConfig;
import co.real.productionreal2.model.response.REBaseResponse;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by kelvinsun on 20/5/16.
 */
public class REApiClient {

    protected static long CONNECTION_TIMEOUT = 10;
    protected static long READ_TIMEOUT = 10;
    protected static long WRITE_TIMEOUT = 10;
    public static final String BASE_URL= REAppConfig.getRealApiBaseUrl();

    public static REApiInterface reApiService;

    private Context context;
    private ReOverrideLoginHandler overriderHandler;

    public REApiClient(Context context, ReOverrideLoginHandler overriderHandler) {
        this.context = context;
        this.overriderHandler = overriderHandler;
        setupApiClient();
    }

    public void setupApiClient() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        reApiService = retrofit.create(REApiInterface.class);
    }

    /**
     * to determine the success case
     * @param errorCode
     * //0-General OK, 20- Login as return member, 21-Login as return member on new device, 22-Login as new member
     * @return
     */
    public static boolean checkResponseSuccess(int errorCode){
        return (errorCode==0||errorCode==20||errorCode==21||errorCode==22);
    }

    /**
     * to determine the loghout case
     * @param errorCode
     * //4 - Access error - access token not found or expired
     * @return
     */
    public static boolean checkIsUserOverride(int errorCode){
        if (errorCode==4) {
            return true;
        }else{
            return false;
        }
    }


    public Callback<REBaseResponse> getJSONResponseCallBack(final REApiCallback callback) {
        return new Callback<REBaseResponse>() {

            @Override
            public void onResponse(Call<REBaseResponse> call, Response<REBaseResponse> response) {
                Log.d("onResponse", "onResponse: " + response.body().toString());
                // at this point the JSON body has been successfully parsed
                if (response.body() != null) {
                    REBaseResponse baseRsp = response.body();
                    int statusCode = baseRsp.errorCode;
                    if (response.isSuccessful() && checkResponseSuccess(statusCode)) {
                        callback.success(baseRsp);
                    } else {
                        if (checkIsUserOverride(statusCode)) {
                            //logout user
                            if (callback instanceof Activity) {
                                overriderHandler.onUserOverride((Activity) callback);
                            }
                        } else {
                            //api error
                            REApiError reApiError = new REApiError(statusCode, String.valueOf(baseRsp.errorCode));
                            callback.failure(reApiError);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<REBaseResponse> call, Throwable t) {
                Log.d("onResponse", "onFailure: " + call);
                callback.failure(null);
            }
        };
    }


    public static REApiInterface getReApiService() {

        return reApiService;
    }

    //error handler
    public static interface ReOverrideLoginHandler {
        public void onUserOverride(Activity activity);

    }
}
