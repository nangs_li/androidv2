package co.real.productionreal2.model;

import android.os.Parcel;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by kelvinsun on 8/8/16.
 */
public class REAddressSearchResult {

    @SerializedName("MID")
    public int mID;
    @SerializedName("MVersion")
    public int mVersion;
    @SerializedName("LID")
    public int lID;
    @SerializedName("LVersion")
    public int lVersion;
    @Nullable
    @SerializedName("Address")
    public List<REAddressWithLocale> address;

    public REAddressSearchResult(Parcel source) {
        mID = source.readInt();
        mVersion = source.readInt();
        lID = source.readInt();
        lVersion = source.readInt();
        address =source.readParcelable(REAddressWithLocale.class.getClassLoader());
    }


    private class REAddressWithLocale {
        @SerializedName("Lang")
        public String lang;
        @SerializedName("Address")
        public String address;

        @Override
        public String toString() {
            return "REAddressWithLocale{" +
                    "lang='" + lang + '\'' +
                    ", address='" + address + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "REAddressSearchResult{" +
                "mID=" + mID +
                ", mVersion=" + mVersion +
                ", lID=" + lID +
                ", lVersion=" + lVersion +
                ", address=" + address +
                '}';
    }

}
