package co.real.productionreal2.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.WindowManager;

import co.real.productionreal2.R;
import co.real.productionreal2.utils.NavigationUtil;
import io.realm.Realm;

public class BaseActivity extends FragmentActivity {

    protected String TAG = this.getClass().getSimpleName();
    BroadcastReceiver networkStateReceiver;
    private NavigationUtil.NavigationType navigationType;
    protected Activity activity;
    protected Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        this.navigationType = NavigationUtil.getNavigationTypeFromIntent(getIntent());
        this.activity = this;
//        realm = ReDatabaseManager.getInstance(this);
        realm = Realm.getDefaultInstance();

        networkStateReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive","Network Type Changed");
            }
        };

        IntentFilter mFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkStateReceiver,mFilter);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        realm.close();

        unregisterReceiver(networkStateReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void finish() {
        super.finish();

        /**
         * close activity animation
         */
        if (this.navigationType == NavigationUtil.NavigationType.SlideUp) {
            //From SlideUp Animation
            overridePendingTransition(R.anim.fade_in, R.anim.slide_bottom_down);
        } else if (this.navigationType == NavigationUtil.NavigationType.SlideIn) {
            //From SlideIn Animation
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else if (this.navigationType == NavigationUtil.NavigationType.None) {
            //From None Animation
            overridePendingTransition(0, 0);
        } else if (this.navigationType == NavigationUtil.NavigationType.Fade) {
            //From Fade Animation
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else {
            //Default == NavigationType.SlideUp
            overridePendingTransition(R.anim.fade_in, R.anim.slide_bottom_down);
        }
    }

}
