package co.real.productionreal2.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by kelvinsun on 27/5/16.
 */
public class NetworkConnectionReceiver extends BroadcastReceiver {
    private static String TAG = "NetworkConnectionReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        String message = isConnected ? "Connect" : "Disconnect";
        Log.d(TAG, message);
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();


    }

}
