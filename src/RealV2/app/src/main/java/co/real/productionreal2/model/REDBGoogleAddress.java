package co.real.productionreal2.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by kelvinsun on 15/7/16.
 */
public class REDBGoogleAddress extends RealmObject{
    @PrimaryKey
    public String placeId;
    public String name;
    public long createdTime;

    public REDBGoogleAddress() {
    }

    public REDBGoogleAddress(String placeId, String name) {
        this.placeId = placeId;
        this.name = name;
        this.createdTime = -System.currentTimeMillis();;
    }

    @Override
    public String toString() {
        return "REDBGoogleAddress{" +
                "placeId='" + placeId + '\'' +
                ", name='" + name + '\'' +
                ", createdTime=" + createdTime +
                '}';
    }
}
