package co.real.productionreal2.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.real.productionreal2.R;
import co.real.productionreal2.REApplication;
import co.real.productionreal2.adapter.EndlessScrollListener;
import co.real.productionreal2.adapter.ListingSearchAdapter;
import co.real.productionreal2.model.REAddressSearchResult;
import co.real.productionreal2.model.REAgentListing;
import co.real.productionreal2.model.REAgentProfile;
import co.real.productionreal2.model.REDBGoogleAddress;
import co.real.productionreal2.model.request.REReqAddressSearch;
import co.real.productionreal2.model.request.REReqAgentListing;
import co.real.productionreal2.model.request.REReqAgentProfile;
import co.real.productionreal2.model.response.REBaseResponse;
import co.real.productionreal2.network.REApiCallback;
import co.real.productionreal2.network.REApiError;
import io.realm.Realm;

/**
 * Created by kelvinsun on 14/7/16.
 */
public class ListingSearchFragment extends BaseFragment implements AdapterView.OnItemClickListener, ListingSearchAdapter.ListingSearchAdapterListener {

    private final String TAG = getClass().getName();
    RecyclerView rvAgentListing;
    TextView tvEmptyMsg;
    TextView tvResponse;

    ListingSearchAdapter listingSearchAdapter;

    List<REAgentProfile> agentProfileList;
    List<REAgentListing> agentListingList;
    List<REAddressSearchResult> recordSet;
    private int PAGE_NO = 0;
    private int OFFSET = 5;
    private boolean PAGE_END = false;

    private Realm realm;


    // Container Activity must implement this interface
    public interface ListingSearchListener {
        public void onSearchResult();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_listing_search, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvEmptyMsg = (TextView) view.findViewById(R.id.tv_empty_msg);
        tvResponse = (TextView) view.findViewById(R.id.tv_response);
        rvAgentListing = (RecyclerView) view.findViewById(R.id.rv_agentlisting);

        initRecyclerview();
    }

    private void initRecyclerview() {
        rvAgentListing.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rvAgentListing.setLayoutManager(llm);
        rvAgentListing.setItemAnimator(new DefaultItemAnimator());
        rvAgentListing.addOnScrollListener(new EndlessScrollListener(llm) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                Log.d("onLoadMore", "onLoadMore: " + page + " -> "+PAGE_NO + " " +PAGE_END);
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                if (!PAGE_END) {
                    customLoadMoreDataFromApi(PAGE_NO);
                }
            }
        });
    }

    private void customLoadMoreDataFromApi(int page) {
//            callAgentProfileGet(getPageIds(PAGE_NO));
    }


    public void callAddressSearch(String name, String placeId) {
        //save latest address search result
        saveSearchResult(name, placeId);
//        placeId="ChIJCzYy5IS16lQRQrfeQ5K5Oxw"; //Dummy test
        REReqAddressSearch reReqAddressSearch = new REReqAddressSearch(1, "1", "1", 1, placeId);
        Log.d("callAddressSearch", "callAddressSearch: success: " + reReqAddressSearch.toString());
        reReqAddressSearch.callAddressSearch(REApplication.get().getReApiClient(), new REApiCallback() {
            @Override
            public void success(REBaseResponse baseResponse) {
                Log.d(TAG, "callAddressSearch: success: " + baseResponse.toString());
                Log.d(TAG, "callAddressSearch: success: " + baseResponse.fullRecordSet);

                tvEmptyMsg.setVisibility(baseResponse.fullRecordSet.size() > 0 ? View.GONE : View.VISIBLE);
//                tvResponse.setVisibility(baseResponse.fullRecordSet.size()>0?View.VISIBLE:View.GONE);
//                tvResponse.setText(baseResponse.toString());

                PAGE_NO = 0;
                PAGE_END = false;

                //reset agentListingList
                agentListingList = new ArrayList<>();
                agentProfileList = new ArrayList<>();
                listingSearchAdapter= null;

                recordSet = baseResponse.fullRecordSet;
                //TODO call agentListingGet and agentProfileGet
                if (recordSet.size() > 0) {
                    callAgentProfileGet(getPageIds(PAGE_NO));
                } else {
                    updateListingView(new ArrayList<REAgentListing>());
                }
            }

            @Override
            public void failure(REApiError REApiError) {
                Log.d(TAG, "callAddressSearch: " + REApiError);

            }
        });
    }

    private List<REAddressSearchResult> getPageIds(int page) {
        Log.d("getPageIds", "start: " + page * OFFSET + " to: " + (page + 1) * OFFSET + " end: " + recordSet.size());
        if ((page + 1) * OFFSET > recordSet.size()) {
            PAGE_END = true;
            return recordSet.subList(page * OFFSET, recordSet.size());
        } else {
            PAGE_END = false;
            return recordSet.subList(page * OFFSET, (page + 1) * OFFSET);
        }
    }

    private void saveSearchResult(String name, String placeId) {
        realm = Realm.getDefaultInstance();

        //convert Object to realm modal
        REDBGoogleAddress googleAddress = new REDBGoogleAddress(placeId, name);
        Log.d(TAG, "saveSearchResult" + googleAddress.toString());

        realm.beginTransaction();
        realm.copyToRealmOrUpdate(googleAddress);
        realm.commitTransaction();
    }

    private void callAgentProfileGet(final List<REAddressSearchResult> addressSearchResultList) {
        REReqAgentProfile reReqAgentProfile = new REReqAgentProfile(1, "1", "1", 1, extractMId(addressSearchResultList));
        reReqAgentProfile.callAgentProfileGet(REApplication.get().getReApiClient(), new REApiCallback() {
                    @Override
                    public void success(REBaseResponse baseResponse) {
                        Log.d(TAG, "callAgentProfileGet: success: " + baseResponse.toString());

                        if (agentProfileList == null) {
                            agentProfileList = new ArrayList<>();
                        }
                        if (baseResponse.agentProfiles != null) {
                            agentProfileList.addAll(baseResponse.agentProfiles);
                            Log.d(TAG, "callAgentProfileGet: success: " + agentProfileList.toString());
                            //get listing
//                            callAgentListingGet(getPageIds(PAGE_NO));
                        }
                    }

                    @Override
                    public void failure(REApiError REApiError) {
                        Log.d(TAG, "callAgentProfileGet: " + REApiError);
                    }
                }

        );
    }

    private List<Integer> extractMId(List<REAddressSearchResult> addressSearchResultList){
        List<Integer> resultList=new ArrayList<>();
        for (REAddressSearchResult reAddressSearchResult:addressSearchResultList){
            resultList.add(reAddressSearchResult.mID);
        }
        return resultList;
    }

    private List<Integer> extractLId(List<REAddressSearchResult> addressSearchResultList){
        List<Integer> resultList=new ArrayList<>();
        for (REAddressSearchResult reAddressSearchResult:addressSearchResultList){
            resultList.add(reAddressSearchResult.mID);
        }
        return resultList;
    }

    private void callAgentListingGet(List<Integer> agentList) {
        REReqAgentListing reReqAgentListing = new REReqAgentListing(1, "1", "1", 1, agentList);
        reReqAgentListing.callAgentListingGet(REApplication.get().getReApiClient(), new REApiCallback() {
            @Override
            public void success(REBaseResponse baseResponse) {
                Log.d(TAG, "callAgentListingGet: success: " + baseResponse.toString());
                Log.d(TAG, "callAgentListingGet: success: " + baseResponse.agentListings);

                if (agentListingList == null) {
                    agentListingList = new ArrayList<REAgentListing>();
                }
                agentListingList.addAll(baseResponse.agentListings);
                updateListingView(agentListingList);
                PAGE_NO++;
            }


            @Override
            public void failure(REApiError REApiError) {
                Log.d(TAG, "callAgentListingGet: " + REApiError);
            }
        });
    }

    private void updateListingView(List<REAgentListing> agentListingList) {

        Log.d(TAG, "updateListingView: " + listingSearchAdapter+ " "+PAGE_NO);
        Log.d(TAG, "updateListingView: " + agentListingList.size());
        if (listingSearchAdapter == null ) {
            listingSearchAdapter = new ListingSearchAdapter(getActivity(), this, this);
            rvAgentListing.setAdapter(listingSearchAdapter);
        } else {
            listingSearchAdapter.notifyItemRangeChanged(0, agentListingList.size());
        }

        initRecyclerview();

    }

    @Override
    public List<REAgentListing> getAgentListingList() {
        return agentListingList;
    }

    @Override
    public List<REAgentProfile> getAgentProfileList() {
        return agentProfileList;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}
