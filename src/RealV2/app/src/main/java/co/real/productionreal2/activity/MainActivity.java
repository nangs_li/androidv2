package co.real.productionreal2.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.path.android.jobqueue.JobManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.real.productionreal2.R;
import co.real.productionreal2.REApplication;
import co.real.productionreal2.config.REAppConfig;
import co.real.productionreal2.event.MessageEvent;
import co.real.productionreal2.jobs.CallApiJob;
import co.real.productionreal2.service.ScheduledThread;
import co.real.productionreal2.utils.AppUtil;
import co.real.productionreal2.utils.NavigationUtil;

public class MainActivity extends BaseActivity {

    @BindView(R.id.tv_main_title)
    TextView tv_MainTitle;
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.tv_request)
    TextView tvRequest;

    private boolean isConnected = false;
    private boolean isEdit = false;
    JobManager jobManager;
    private NetworkChangeReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String envString = REAppConfig.IS_DEV ? "Development" : "Production";
        Log.d(this.TAG, "Build environment = " + envString);

        setContentView(R.layout.activity_main_test);

        ButterKnife.bind(this);

        jobManager = REApplication.get().getJobManager();
        initData();
        initView();
        addListener();

        receiver = new NetworkChangeReceiver();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initData() {
    }

    private void initView() {
        tv_MainTitle.setText("Set text by function");
    }

    private void addListener() {

    }


    @OnClick(R.id.bn_main_hello) void sayHello() {
        AppUtil.showToast(this, "STARTED");
        ScheduledThread.startJob(this);
    }

    @OnClick(R.id.bn_main_stop) void stopTask() {

        AppUtil.showToast(this, "STOP");
        ScheduledThread.clearJob();
    }

    @OnClick(R.id.bn_main_startactivity)
    void clickStartActivity() {
        Intent intent = new Intent(activity, HostActivity.class);
        NavigationUtil.startActivity(activity, intent);
    }

    @OnClick(R.id.bn_main_startactivityforresult)
    void clickStartActivityForResult() {
        Intent intent = new Intent(activity, TestingActivity.class);
        NavigationUtil.startActivityForResult(activity, intent, 0, NavigationUtil.NavigationType.SlideIn);
    }

    @OnClick(R.id.bn_main_start_testingdb_activity)
    void clickStartTestingDBActivity() {
        Intent intent = new Intent(activity, RemoteDemoActivity.class);
        NavigationUtil.startActivity(activity, intent, NavigationUtil.NavigationType.None);
    }

    @OnClick(R.id.bn_api)
    void callApi() {
        tvContent.setText("Loading...");
        jobManager.addJobInBackground(new CallApiJob());
        //api sample

//        ReqLogin reqLogin=new ReqLogin("123",2,"kelivnsun",1,"kelvin.sun@hohojo.com","123888","http://hk.yahoo.com","en","1241234");
//        tvRequest.setText(reqLogin.toString());
//        reqLogin.callLogin(REApplication.get().getReApiClient(), new REApiCallback() {
//            @Override
//            public void success(BaseResponse baseResponse) {
//                RspLogin rspLogin = new Gson().fromJson(baseResponse.jsonContent, RspLogin.class);
//                if (rspLogin!=null) {
//                    tvContent.setText(rspLogin.toString());
//                }
//            }
//
//            @Override
//            public void failure(REApiError reApiError) {
//                if (reApiError!=null) {
//                    tvContent.setText(reApiError.toString());
//                }else{
//                    tvContent.setText("Network not available");
//                }
//            }
//        });

    }

    @OnClick(R.id.bn_api_fail)
    void callApiFail() {
        tvContent.setText("Loading...");
        //api sample
//        REReqLogin REReqLogin =new REReqLogin("123",2,"kelivnsun",1,"kelvin.sun@hohojo.com","123888","","en","1241234");
//              tvRequest.setText(REReqLogin.toString());
//        REReqLogin.callLogin(this, new REApiCallback() {
//
//            @Override
//            public void success(BaseResponse baseResponse) {
//                RspLogin rspLogin = new Gson().fromJson(baseResponse.jsonContent, RspLogin.class);
//                if (rspLogin!=null) {
//                    tvContent.setText("SUCCESS "+rspLogin.toString());
//                }
//            }
//
//            @Override
//            public void failure(REApiError REApiError) {
//                if (REApiError!=null) {
//                    tvContent.setText("FAIL "+REApiError.toString());
//                }else{
//                    tvContent.setText("Network not available");
//
//                }
//            }
//        });
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    // This method will be called when a MessageEvent is posted
    @Subscribe
    public void onMessageEvent(MessageEvent event){
        Date currentDate = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        final String currentDateandTime = sdf.format(currentDate);
        tvRequest.setText(currentDateandTime);
        tvContent.setText(event.message);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            EventBus.getDefault().unregister(this);
            unregisterReceiver(networkStateReceiver);
        } catch (Throwable t){
            //this may crash if registration did not go through. just be safe
        }
    }

    public class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {

            Log.d("onReceive","Receieved notification about network status");
            isNetworkAvailable(context);

        }


        private boolean isNetworkAvailable(Context context) {
            ConnectivityManager connectivity = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null) {
                    for (int i = 0; i < info.length; i++) {
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            if (!isConnected) {
                                tv_MainTitle.setTextColor(getResources().getColor(R.color.holo_blue));
                                tv_MainTitle.setText("CONNECTED");
                                isConnected = true;
                                //do your processing here ---
                                //if you need to post any data to the server or get status
                                //update from the server
                            }
                            return true;
                        }
                    }
                }
            }
            tv_MainTitle.setTextColor(getResources().getColor(R.color.red));
            tv_MainTitle.setText("DISCONNECTED");
            isConnected = false;
            return false;
        }
    }

}
