package co.real.productionreal2.jobs;

import android.util.Log;

import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.List;

import co.real.productionreal2.REConstants;
import co.real.productionreal2.model.db.REAPIQueue;
import co.real.productionreal2.model.db.REDBAPIQueue;
import co.real.productionreal2.model.db.RealmInteger;
import co.real.productionreal2.model.response.BaseResponse;
import co.real.productionreal2.utils.DataUtil;
import io.realm.Realm;

/**
 * Created by kelvinsun on 26/5/16.
 */
public class CallApiJob extends Job {
    public long queueId;
    public int httpMethod;
    public List<Integer> dependingQueues;
    public List<Integer> pendingQueues;
    public int retryCount;
    public BaseResponse response;
    public String url;
    public String parameter;
    public REConstants.API_QUEUE_STATUS status;

    protected Realm realm;

    public CallApiJob(REAPIQueue reapiQueue,int priority) {
        super(new Params(priority).requireNetwork().persist());//order of tweets matter, we don't want to send two in parallel
        //use a negative id so that it cannot collide w/ twitter ids
        //we have to set local id here so it gets serialized into job (to find tweet later on)
        queueId = -System.currentTimeMillis();
        this.url = reapiQueue.url;
        this.dependingQueues = reapiQueue.dependingQueues;
        this.pendingQueues = reapiQueue.pendingQueues;
        this.retryCount = reapiQueue.retryCount;
        this.response = reapiQueue.response;
        this.url = reapiQueue.url;
        this.parameter = reapiQueue.parameter;
        this.status = reapiQueue.status;
    }

    public CallApiJob() {
        super(new Params(Priority.MID).requireNetwork().persist());//order of tweets matter, we don't want to send two in parallel
        //api sample
    }

    @Override
    public void onAdded() {
        Log.d("CallApiJob", "onAdded");
        //job has been secured to disk, add item to database
        realm = Realm.getDefaultInstance();

        //convert Object to realm modal
        REDBAPIQueue queueJob = new REDBAPIQueue();
        queueJob.queueId = queueId;
        queueJob.url = url;
        if (dependingQueues != null) {
            for (Integer dQueue : dependingQueues) {
                RealmInteger realmInteger = new RealmInteger(dQueue);
                queueJob.dependingQueues.add(realmInteger);
            }
        }
        if (pendingQueues != null) {
            for (Integer pQueue : pendingQueues) {
                RealmInteger realmInteger = new RealmInteger(pQueue);
                queueJob.pendingQueues.add(realmInteger);
            }
        }
        queueJob.retryCount = retryCount;
        queueJob.url = url;
        queueJob.parameter = parameter;
        queueJob.status = status.ordinal();

        Log.d("CallApiJob", "onAdded" + queueJob.toString());

        realm.beginTransaction();
        realm.copyToRealmOrUpdate(queueJob);
        realm.commitTransaction();

//            REQueueModel.getInstance().insertOrReplace(queueJob);
//            EventBus.getDefault().post(new PostingTweetEvent(queueJob));
    }

    @Override
    public void onRun() throws Throwable {
        Log.d("CallApiJob", "onRun getCurrentRunCount: " + getCurrentRunCount());
//        Status status = QueueJobController.getInstance().postTweet(text);
//        REQueueJob newQueueJob = new REQueueJob(status);
//        REQueueModel queueModel = REQueueModel.getInstance();
//        REQueueJob existingJob = queueModel.getTweetByLocalId(localId);
//        if(existingJob != null) {
//            existingJob.updateNotNull(newQueueJob);
//            //don't set local to false. this way, next time we ask for history update, we'll send proper tweet id
//            queueModel.insertOrReplace(existingJob);
//        } else {
//            //somewhat local tweet does not exist. we might have crashed before onAdded is called.
//            //just insert as if it is a new tweet
//            queueModel.insertOrReplace(newQueueJob);
//        }
//        EventBus.getDefault().post(new PostedTweetEvent(newTweet, localId));


        REAPIQueue currentQueue = DataUtil.getQueueById(queueId);

        if (currentQueue != null) {
            OkHttpClient client = new OkHttpClient();

            String reqContent=parameter;
            String reqUrl=url;

            //TEST only
            reqContent="{\n" +
                    "\"ApiName\": \"AddressSearch\", \"LanguageIndex\": 1, \"UniqueKey\": \"546456\",\n" +
                    "\"AccessToken\": \"b7620441-fb00-4b95-9e73-3666a19bfacb\", \"MemberID\": 158,\n" +
                    "\"GoogleAddress\": {\n" +
                    "\"formatted_address\": \"United States\", \"address_components\": [\n" +
                    "{\n" +
                    "\"long_name\": \"United states\", \"short_name\": \"US\",\n" +
                    "\"types\": [\n" +
                    "\"country\",\n" +
                    "\"political\" ]\n" +
                    "} ],\n" +
                    "\"place_id\": \"ChIJCzYy5IS16lQRQrfeQ5K5Oxw\" }\n" +
                    "}\"";
            reqUrl="http://dev.real.co:8888/v2/Listing";

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, reqContent);
            Request request = new Request.Builder()
                    .url(reqUrl)
                    .post(body)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .addHeader("postman-token", "a91079a0-fa50-62fe-14c1-8eb73232ef10")
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {
                    Log.d("CallApiJob", request.body().toString());
                    REDBAPIQueue currentQueue = DataUtil.getDBQueueById(queueId);
                    if (currentQueue != null) {
                    realm=Realm.getDefaultInstance();
                    realm.beginTransaction();
                    currentQueue.status= 2; //Fail
                    currentQueue.parameter= "FAIL";
                        realm.copyToRealmOrUpdate(currentQueue);
                        Log.d("CallApiJob", "onAdded DONE: " +  DataUtil.getDBQueueById(queueId));
                    } else {

                    }
                    realm.commitTransaction();
                }

                @Override
                public void onResponse(Response response) throws IOException {
                    Log.d("CallApiJob", response.toString());
                    REDBAPIQueue currentQueue = DataUtil.getDBQueueById(queueId);
                    if (currentQueue != null) {
                    realm=Realm.getDefaultInstance();
                    realm.beginTransaction();
                    currentQueue.status= 1; //Success
                    currentQueue.parameter= "DONE";
                        realm.copyToRealmOrUpdate(currentQueue);
                        Log.d("CallApiJob", "onAdded DONE: " +  DataUtil.getDBQueueById(queueId));
                    } else {

                    }
                    realm.commitTransaction();
                }
            });
        } else {
            Log.d("CallApiJob", "no current job");
        }

//        Log.d("CallApiJob", "CallApiJob");
//        REReqLogin reqLogin = new REReqLogin("123", 2, "kelivnsun", 1, "kelvin.sun@hohojo.com", "123888", "http://hk.yahoo.com", "en", "1241234");
//        reqLogin.callLogin(REApplication.get().getReApiClient(), new REApiCallback() {
//            @Override
//            public void success(BaseResponse baseResponse) {
//                RspLogin rspLogin = new Gson().fromJson(baseResponse.jsonContent, RspLogin.class);
//                if (rspLogin != null) {
//                    EventBus.getDefault().post(new MessageEvent(rspLogin.toString()));
//                }
//                Log.d("CallApiJob", "CallApiJob success");
//            }
//
//            @Override
//            public void failure(REApiError reApiError) {
//                Log.d("CallApiJob", "CallApiJob reApiError");
//                if (reApiError != null) {
//                    EventBus.getDefault().post(new MessageEvent(reApiError.toString()));
//                } else {
//                    EventBus.getDefault().post(new MessageEvent("Network not available"));
//                }
//            }
//        });
    }

    @Override
    protected void onCancel() {
        //delete local tweet
//        REQueueJob localJob = REQueueModel.getInstance().getTweetByLocalId(localId);
//        if(localJob != null) {
//            REQueueModel.getInstance().deleteTweetById(localId);
//            EventBus.getDefault().post(new DeletedTweetEvent(localId));
//        }
    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
//        if(throwable instanceof TwitterException) {
//            //if it is a 4xx error, stop
//            TwitterException twitterException = (TwitterException) throwable;
//            return twitterException.getStatusCode() < 400 || twitterException.getStatusCode() > 499;
//        }
        return true;
    }
}
