package co.real.productionreal2.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by kelvinsun on 8/8/16.
 */
public class REExperience extends RealmObject {
    @SerializedName("Company")
    public String company;
    @SerializedName("Title")
    public String title;
    @SerializedName("IsCurrentJob")
    public int isCurrentJob;
    @SerializedName("StartDate")
    public String startDate;
    @SerializedName("EndDate")
    public String endDate;
}
