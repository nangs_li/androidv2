package co.real.productionreal2.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.SearchView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResolvingResultCallbacks;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.real.productionreal2.R;
import co.real.productionreal2.fragment.ListingSearchFragment;
import co.real.productionreal2.fragment.SearchAutocompleteFragment;
import co.real.productionreal2.model.REDBGoogleAddress;
import co.real.productionreal2.utils.DataUtil;

/**
 * Created by kelvinsun on 14/7/16.
 */
public class HostActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener , SearchAutocompleteFragment.SearchAutocompleteAdapterListener{

    private GoogleApiClient mGoogleApiClient;

    public List<REDBGoogleAddress> autocompleteAddressList=new ArrayList<>();

    @BindView(R.id.sv_address)
    SearchView svAddress;

    static SearchAutocompleteFragment searchAutocompleteFragment;
    ListingSearchFragment listingSearchFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_host);
        ButterKnife.bind(this);

        initData();
        initView();
        addListener();

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();
    }

    private void initView() {
        searchAutocompleteFragment = (SearchAutocompleteFragment)
                getSupportFragmentManager().findFragmentById(R.id.fl_overlay_container);

        listingSearchFragment = (ListingSearchFragment)
                getSupportFragmentManager().findFragmentById(R.id.fl_main_container);
    }

    private void queryResults(String query){
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE)
                .build();

//        LatLngBounds bounds=new LatLngBounds(
//                new LatLng(-33.880490, 151.184363),
//                new LatLng(-33.858754, 151.229596));

        PendingResult<AutocompletePredictionBuffer> result =
                Places.GeoDataApi.getAutocompletePredictions(mGoogleApiClient, query,
                        null, typeFilter);
        Log.d("queryResults","queryResults: "+query);
        result.setResultCallback(new ResolvingResultCallbacks<AutocompletePredictionBuffer>(this,1) {
            @Override
            public void onSuccess(@NonNull AutocompletePredictionBuffer autocompletePredictions) {
                Log.d("queryResults","queryResults: "+autocompletePredictions.getCount());
                autocompleteAddressList=new ArrayList<>();
                for (AutocompletePrediction address:autocompletePredictions){
                    Log.d("queryResults","queryResults: "+address.getFullText(null));
                    REDBGoogleAddress googleAddress=new REDBGoogleAddress(address.getPlaceId(),address.getFullText(null).toString());
                    autocompleteAddressList.add(googleAddress);
                }
                onSearchResult(autocompleteAddressList);
                autocompletePredictions.release();
            }

            @Override
            public void onUnresolvableFailure(@NonNull Status status) {
                Log.d("queryResults","queryResults: "+status.toString());
            }
        });


    }

    private void addListener() {
        svAddress.setOnQueryTextListener(queryChangeListener);
        svAddress.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                autocompleteAddressList.clear();
                onSearchResult(autocompleteAddressList);

                return false;
            }
        });
    }

    SearchView.OnQueryTextListener queryChangeListener=new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            //search with the top result
            if (autocompleteAddressList!=null && autocompleteAddressList.size()>0) {
                onAddressSelected(autocompleteAddressList.get(0));
            }
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            autocompleteAddressList=new ArrayList<>();
            if (newText.length()==0) {
                //show latest search history
                autocompleteAddressList= DataUtil.getLatestAddressSearchHistory();
                onSearchResult(autocompleteAddressList);

                Log.d("queryResults","queryResults: "+autocompleteAddressList);
            }else{
                queryResults(newText);
            }
            return false;
        }
    };

    private void initData() {
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onSearchResult(List<REDBGoogleAddress> autocompleteAddressList) {
//        if (searchAutocompleteFragment==null){
//            searchAutocompleteFragment = (SearchAutocompleteFragment)
//                    getSupportFragmentManager().findFragmentById(R.id.fl_overlay_container);
//        }
        //show overlay
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.show(searchAutocompleteFragment);
        ft.commit();

        if (searchAutocompleteFragment != null && autocompleteAddressList!=null) {
            // If article frag is available, we're in two-pane layout...

            // Call a method in the ArticleFragment to update its content
            searchAutocompleteFragment.updateView(autocompleteAddressList);
        }
    }

    @Override
    public void onAddressSelected(REDBGoogleAddress address) {
//        searchAutocompleteFragment.onDetach();
        svAddress.setOnQueryTextListener(null);
        svAddress.setQuery(address.name,true);
        svAddress.setOnQueryTextListener(queryChangeListener);

        listingSearchFragment.callAddressSearch(address.name.toString(),address.placeId);

        //hide overlay
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.hide(searchAutocompleteFragment);
        ft.commit();
    }


}
