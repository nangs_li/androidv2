package co.real.productionreal2.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResolvingResultCallbacks;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.real.productionreal2.R;
import co.real.productionreal2.REApplication;
import co.real.productionreal2.adapter.EndlessScrollListener;
import co.real.productionreal2.adapter.ListingSearchAdapter;
import co.real.productionreal2.adapter.SearchAutocompleteAdapter;
import co.real.productionreal2.controller.HidingScrollController;
import co.real.productionreal2.model.REAddressSearchResult;
import co.real.productionreal2.model.REAgentListing;
import co.real.productionreal2.model.REAgentProfile;
import co.real.productionreal2.model.REDBGoogleAddress;
import co.real.productionreal2.model.request.REReqAddressSearch;
import co.real.productionreal2.model.request.REReqAgentListing;
import co.real.productionreal2.model.request.REReqAgentProfile;
import co.real.productionreal2.model.response.REBaseResponse;
import co.real.productionreal2.network.REApiCallback;
import co.real.productionreal2.network.REApiError;
import co.real.productionreal2.utils.AppUtil;
import co.real.productionreal2.utils.DataUtil;
import customui.SimpleDividerItemDecoration;
import io.realm.Realm;

/**
 * Created by kelvinsun on 5/8/16.
 */
public class RESearchMainFragment extends BaseFragment implements GoogleApiClient.OnConnectionFailedListener,
        ListingSearchAdapter.ListingSearchAdapterListener {

    private final String TAG = getClass().getName();

    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.rv_address)
    RecyclerView rvAddressList;
    @BindView(R.id.rv_agentlisting)
    RecyclerView rvAgentListing;
    SearchAutocompleteAdapter searchAutocompleteAdapter;
    SearchView svAddress;

    private static final String SECTION_NAME = "SECTION_NAME";

    private String sectionName;

    public List<REDBGoogleAddress> autocompleteAddressList = new ArrayList<>();


    ListingSearchAdapter listingSearchAdapter;

    List<REAgentProfile> agentProfileList;
    List<REAgentListing> agentListingList;
    List<REAddressSearchResult> recordSet;

    private int PAGE_NO = 0;
    private int OFFSET = 5;
    private boolean PAGE_END = false;

    private Realm realm;

    /**
     * Create a new instance of CountingFragment, providing "num"
     * as an argument.
     */
    public static RESearchMainFragment newInstance(String sectionName) {
        RESearchMainFragment f = new RESearchMainFragment();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("SECTION_NAME", sectionName);
        f.setArguments(args);
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_search, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(getActivity());
        if (getArguments() != null) {
            sectionName = getArguments().getString(SECTION_NAME);
        }

        initView();
        addListener();

    }
    private void initView() {

        svAddress = getMainFragment().titleBar.getSvSearch();
        tvContent.setText(sectionName);

        //init recycler view
        rvAddressList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rvAddressList.setLayoutManager(llm);
        rvAddressList.setItemAnimator(new DefaultItemAnimator());
        rvAddressList.addItemDecoration(new SimpleDividerItemDecoration(getResources()));

    }

    private void initAgentListingRecyclerview() {
        rvAgentListing.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rvAgentListing.setLayoutManager(llm);
        rvAgentListing.setItemAnimator(new DefaultItemAnimator());
        rvAgentListing.addOnScrollListener(new EndlessScrollListener(llm) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                Log.d("onLoadMore", "onLoadMore: " +      page + " -> "+PAGE_NO + " " +PAGE_END);
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                if (!PAGE_END) {
                    customLoadMoreDataFromApi();
                }
            }
        });
    }

    private void customLoadMoreDataFromApi() {
            callAgentProfileGet(getPageIds(PAGE_NO));
    }

    private void addListener() {
        svAddress.setOnQueryTextListener(queryChangeListener);
        svAddress.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                getMainFragment().tabBar.show(!hasFocus);
            }
        });
        svAddress.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                autocompleteAddressList.clear();
                updateView(autocompleteAddressList);

                return false;
            }
        });


        rvAgentListing.addOnScrollListener(new HidingScrollController() {
            @Override
            public void onHide() {
                getMainFragment().titleBar.hide();
                getMainFragment().tabBar.hide();
            }

            @Override
            public void onShow() {
                getMainFragment().titleBar.show();
                getMainFragment().tabBar.show();
            }
        });
    }

    SearchView.OnQueryTextListener queryChangeListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            //search with the top result
            if (autocompleteAddressList != null && autocompleteAddressList.size() > 0) {

                REDBGoogleAddress address = autocompleteAddressList.get(0);
                svAddress.setOnQueryTextListener(null);
                svAddress.setQuery(address.name, true);
                svAddress.setOnQueryTextListener(queryChangeListener);

                //call api
                callAddressSearch(address.name.toString(),address.placeId);

            }
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            autocompleteAddressList = new ArrayList<>();
            if (newText.length() == 0) {
                //show latest search history
                autocompleteAddressList = DataUtil.getLatestAddressSearchHistory();
                updateView(autocompleteAddressList);

                Log.d("queryResults", "queryResults: " + autocompleteAddressList);
            } else {
                queryResults(newText);
            }

            getMainFragment().titleBar.hideThreeDots();

            return false;
        }
    };

    private void queryResults(String query) {
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE)
                .build();

//        LatLngBounds bounds=new LatLngBounds(
//                new LatLng(-33.880490, 151.184363),
//                new LatLng(-33.858754, 151.229596));

        PendingResult<AutocompletePredictionBuffer> result =
                Places.GeoDataApi.getAutocompletePredictions(REApplication.get().mGoogleApiClient, query,
                        null, typeFilter);
        Log.d("queryResults", "queryResults: " + query);
        result.setResultCallback(new ResolvingResultCallbacks<AutocompletePredictionBuffer>(getActivity(), 1) {
            @Override
            public void onSuccess(@NonNull AutocompletePredictionBuffer autocompletePredictions) {
                Log.d("queryResults", "queryResults: " + autocompletePredictions.getCount());
                autocompleteAddressList = new ArrayList<>();
                for (AutocompletePrediction address : autocompletePredictions) {
                    Log.d("queryResults", "queryResults: " + address.getFullText(null));
                    REDBGoogleAddress googleAddress = new REDBGoogleAddress(address.getPlaceId(), address.getFullText(null).toString());
                    autocompleteAddressList.add(googleAddress);

                    tvContent.setText(address.getFullText(null));
                }
                updateView(autocompleteAddressList);
                autocompletePredictions.release();
            }

            @Override
            public void onUnresolvableFailure(@NonNull Status status) {
                Log.d("queryResults", "queryResults: " + status.toString());
            }
        });


    }


    public void updateView(List<REDBGoogleAddress> autocompleteAddressList) {
        searchAutocompleteAdapter = new SearchAutocompleteAdapter(autocompleteAddressList);
        rvAddressList.setAdapter(searchAutocompleteAdapter);

        searchAutocompleteAdapter.setOnItemClickListener(itemClickListener);
    }

    AdapterView.OnItemClickListener itemClickListener=new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            REDBGoogleAddress address = searchAutocompleteAdapter.getItem(position);
            AppUtil.showToast(getActivity(), address.placeId);
            Log.d("getPlaceId", "getPlaceId: " + address.placeId);
            //clear search results
            updateView(new ArrayList<REDBGoogleAddress>());

            //TODO: call address search api
            //update searchview
            svAddress.setOnQueryTextListener(null);
            svAddress.setQuery(address.name, true);
            svAddress.setOnQueryTextListener(queryChangeListener);

            //call api
            callAddressSearch(address.name.toString(),address.placeId);
        }
    };


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onResume() {
        super.onResume();

        realm = Realm.getDefaultInstance();

//        mGoogleApiClient = new GoogleApiClient
//                .Builder(getActivity())
//                .addApi(Places.GEO_DATA_API)
//                .addApi(Places.PLACE_DETECTION_API)
//                .enableAutoManage(getActivity(), this)
//                .build();

        //TODO: testing
        Log.d("loadFromDB","loadfromDB: "+DataUtil.getAgentProfileList());
    }

    @Override
    public void onPause() {
        super.onPause();
//        mGoogleApiClient.stopAutoManage(getActivity());
//        mGoogleApiClient.disconnect();
    }


    public void callAddressSearch(String name, String placeId) {
        //show 3 dots
        getMainFragment().titleBar.showThreeDots(true);
        getMainFragment().tabBar.show();

        //save latest address search result
        saveSearchResultToDb(name, placeId);
//        placeId="ChIJCzYy5IS16lQRQrfeQ5K5Oxw"; //Dummy test
        REReqAddressSearch reReqAddressSearch = new REReqAddressSearch(1, "1", "1", 1, placeId);
        Log.d("callAddressSearch", "callAddressSearch: success: " + reReqAddressSearch.toString());
        reReqAddressSearch.callAddressSearch(REApplication.get().getReApiClient(), new REApiCallback() {
            @Override
            public void success(REBaseResponse baseResponse) {
                Log.d(TAG, "callAddressSearch: success: " + baseResponse.toString());
                if (baseResponse.fullRecordSet!=null)
                Log.d(TAG, "callAddressSearch: success: " + baseResponse.fullRecordSet.size()+ " "+baseResponse.fullRecordSet);

                tvContent.setText("NO RESULTS");
                tvContent.setVisibility(baseResponse.fullRecordSet==null?View.VISIBLE: View.GONE);
//                tvResponse.setVisibility(baseResponse.fullRecordSet.size()>0?View.VISIBLE:View.GONE);
//                tvResponse.setText(baseResponse.toString());

                PAGE_NO = 0;
                PAGE_END = false;

                //reset agentListingList
                agentListingList = new ArrayList<>();
                agentProfileList = new ArrayList<>();
                listingSearchAdapter= null;

                recordSet = baseResponse.fullRecordSet;
                //TODO call agentListingGet and agentProfileGet
                if (recordSet!=null) {
                    if (recordSet.size() > 0) {
                        callAgentProfileGet(getPageIds(PAGE_NO));
                    } else {
                        updateListingView(new ArrayList<REAgentListing>());
                    }
                }
            }

            @Override
            public void failure(REApiError REApiError) {
                Log.d(TAG, "callAddressSearch: " + REApiError);

            }
        });
    }

    private void saveAgentProfileToDb(REAgentProfile reAgentProfile){
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(reAgentProfile);
        realm.commitTransaction();
    }

    private void saveAgentProfileToDb(List<REAgentProfile> reAgentProfileList){
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(reAgentProfileList);
        realm.commitTransaction();
    }

    private void saveAgentListingToDb(REAgentListing reAgentListing){
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(reAgentListing);
        realm.commitTransaction();
    }

    private void saveAgentListingToDb(List<REAgentListing> reAgentListingList){
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(reAgentListingList);
        realm.commitTransaction();
    }



    private void saveSearchResultToDb(String name, String placeId) {
        //convert Object to realm modal
        REDBGoogleAddress googleAddress = new REDBGoogleAddress(placeId, name);
        Log.d(TAG, "saveSearchResultToDb" + googleAddress.toString());

        realm.beginTransaction();
        realm.copyToRealmOrUpdate(googleAddress);
        realm.commitTransaction();
    }

    private void callAgentProfileGet(final List<REAddressSearchResult> addressSearchResultList) {
        REReqAgentProfile reReqAgentProfile = new REReqAgentProfile(1, "1", "1", 1, extractMId(addressSearchResultList));
        reReqAgentProfile.callAgentProfileGet(REApplication.get().getReApiClient(), new REApiCallback() {
                    @Override
                    public void success(REBaseResponse baseResponse) {
                        Log.d(TAG, "callAgentProfileGet: success: " + baseResponse.toString());

                        if (agentProfileList == null) {
                            agentProfileList = new ArrayList<>();
                        }
                        if (baseResponse.agentProfiles != null) {
                            agentProfileList.addAll(baseResponse.agentProfiles);

                            //save to DB
                            saveAgentProfileToDb(baseResponse.agentProfiles);
                            Log.d(TAG, "callAgentProfileGet: success: " + agentProfileList.toString());
                            //get listing
                            callAgentListingGet(getPageIds(PAGE_NO));
                        }
                    }

                    @Override
                    public void failure(REApiError REApiError) {
                        Log.d(TAG, "callAgentProfileGet: " + REApiError);
                    }
                }

        );
    }

    private List<Integer> extractMId(List<REAddressSearchResult> addressSearchResultList){
        List<Integer> resultList=new ArrayList<>();
        for (REAddressSearchResult reAddressSearchResult:addressSearchResultList){
            resultList.add(reAddressSearchResult.mID);
        }
        return resultList;
    }

    private List<Integer> extractLId(List<REAddressSearchResult> addressSearchResultList){
        List<Integer> resultList=new ArrayList<>();
        for (REAddressSearchResult reAddressSearchResult:addressSearchResultList){
            resultList.add(reAddressSearchResult.lID);
        }
        return resultList;
    }

    private void callAgentListingGet(List<REAddressSearchResult> agentList) {
        REReqAgentListing reReqAgentListing = new REReqAgentListing(1, "1", "1", 1, extractLId(agentList));
        reReqAgentListing.callAgentListingGet(REApplication.get().getReApiClient(), new REApiCallback() {
            @Override
            public void success(REBaseResponse baseResponse) {
                Log.d(TAG, "callAgentListingGet: success: " + baseResponse.toString());
                Log.d(TAG, "callAgentListingGet: success: " + baseResponse.agentListings);

                if (agentListingList == null) {
                    agentListingList = new ArrayList<REAgentListing>();
                }
                agentListingList.addAll(baseResponse.agentListings);
                //save to DB
                saveAgentListingToDb(baseResponse.agentListings);
                updateListingView(agentListingList);
                PAGE_NO++;
            }


            @Override
            public void failure(REApiError REApiError) {
                Log.d(TAG, "callAgentListingGet: " + REApiError);
            }
        });
    }

    private void updateListingView(List<REAgentListing> agentListingList) {

        Log.d(TAG, "updateListingView: " + listingSearchAdapter+ " "+PAGE_NO);
        Log.d(TAG, "updateListingView: " + agentListingList.size());
        if (listingSearchAdapter == null ) {
            initAgentListingRecyclerview();
            listingSearchAdapter = new ListingSearchAdapter(getActivity(), this);
            rvAgentListing.setAdapter(listingSearchAdapter);
        }
        listingSearchAdapter.notifyDataSetChanged();

    }

    private List<REAddressSearchResult> getPageIds(int page) {
        Log.d("getPageIds", "start: " + page * OFFSET + " to: " + (page + 1) * OFFSET + " end: " + recordSet.size());
        if ((page + 1) * OFFSET > recordSet.size()) {
            PAGE_END = true;
            return recordSet.subList(page * OFFSET, recordSet.size());
        } else {
            PAGE_END = false;
            return recordSet.subList(page * OFFSET, (page + 1) * OFFSET);
        }
    }

    @Override
    public List<REAgentListing> getAgentListingList() {
        Log.d(TAG, "notifyDataSetChanged: 1: "  + agentListingList.size());
        return agentListingList;
    }

    @Override
    public List<REAgentProfile> getAgentProfileList() {
        Log.d(TAG, "notifyDataSetChanged: 2: " + agentProfileList.size());
        return agentProfileList;
    }
}
