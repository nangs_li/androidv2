package co.real.productionreal2;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.path.android.jobqueue.JobManager;
import com.path.android.jobqueue.config.Configuration;
import com.path.android.jobqueue.log.CustomLogger;

import java.lang.ref.WeakReference;

import co.real.productionreal2.network.REApiClient;
import co.real.productionreal2.network.REApiInterface;
import co.real.productionreal2.controller.REDatabase;

/**
 * Created by edwinchan on 18/5/16.
 */
public class REApplication extends Application{


    private static WeakReference<REApplication> mAppInstance;
    private static REApiClient reApiClient;
    private JobManager jobManager;

    public GoogleApiClient mGoogleApiClient;


    private REApiClient.ReOverrideLoginHandler mReOverrideHandler = new REApiClient.ReOverrideLoginHandler() {
        @Override
        public void onUserOverride(Activity activity) {
            Log.d("LOGOUT","LOGOUT");
            logoutAndClearData(activity);
        }
    };


    @Override
    public void onCreate() {
        Log.v("","REApplication onCreate");
        super.onCreate();
        REDatabase.init(this);

        configureJobManager();


        if (mAppInstance == null) {
            // create short hand
            mAppInstance = new WeakReference<REApplication>(this);
        }
        reApiClient = new REApiClient(this,mReOverrideHandler);



    }



    public static void logoutAndClearData(Activity activity){
    }

    private void configureJobManager() {
        Configuration configuration = new Configuration.Builder(this)
                .customLogger(new CustomLogger() {
                    private static final String TAG = "JOBS";
                    @Override
                    public boolean isDebugEnabled() {
                        return true;
                    }

                    @Override
                    public void d(String text, Object... args) {
                        Log.d(TAG, String.format(text, args));
                    }

                    @Override
                    public void e(Throwable t, String text, Object... args) {
                        Log.e(TAG, String.format(text, args), t);
                    }

                    @Override
                    public void e(String text, Object... args) {
                        Log.e(TAG, String.format(text, args));
                    }
                })
                .minConsumerCount(1)//always keep at least one consumer alive
                .maxConsumerCount(1)//up to 3 consumers at a time
                .loadFactor(1)//3 jobs per consumer
                .consumerKeepAlive(120)//wait 2 minute
                .build();
        jobManager = new JobManager(this, configuration);
    }

    public JobManager getJobManager() {
        return jobManager;
    }

    public static void logoutAndClearData(Context Context){
}

    public static REApiClient getReApiClient() {
        return reApiClient;
    }

    public static REApiInterface getReApiService() {
        return REApiClient.getReApiService();
    }


    static void resetApiClient() {
        reApiClient = null;
    }

    public static REApplication get() {
        return mAppInstance.get();
    }
}
