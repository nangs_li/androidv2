package co.real.productionreal2.network;

import co.real.productionreal2.model.response.REBaseResponse;

/**
 * Created by kelvinsun on 24/5/16.
 */
public interface REApiCallback {
        public void success(REBaseResponse baseResponse);
        public void failure(REApiError REApiError);
}
