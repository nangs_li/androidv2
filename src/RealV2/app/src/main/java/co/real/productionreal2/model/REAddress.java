package co.real.productionreal2.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by kelvinsun on 8/8/16.
 */
public class REAddress extends RealmObject {
    @SerializedName("Lang")
    public String lang;
    @SerializedName("FormattedAddress")
    public String formattedAddress;
    @SerializedName("ShortAddress")
    public String shortAddress;
    @SerializedName("PlaceID")
    public String placeID;

    @Override
    public String toString() {
        return "Address{" +
                "lang='" + lang + '\'' +
                ", formattedAddress='" + formattedAddress + '\'' +
                ", shortAddress='" + shortAddress + '\'' +
                ", placeID='" + placeID + '\'' +
                '}';
    }
}
