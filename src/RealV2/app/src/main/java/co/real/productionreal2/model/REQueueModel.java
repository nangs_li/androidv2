package co.real.productionreal2.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kelvinsun on 26/5/16.
 */
public class REQueueModel {
        private static REQueueModel instance;
//        private QueueDao queueDao;

        public synchronized static REQueueModel getInstance() {
            if(instance == null) {
                instance = new REQueueModel();
            }
            return instance;
        }

    public List<REQueueJob> loadQueue() {
            List<REQueueJob> reBgQueueList=new ArrayList<>();
        reBgQueueList.add(new REQueueJob(Long.valueOf(1),123,null,null,2,"abc","def","hfg","ijk","egg"));
            return reBgQueueList;

        }

    public REQueueJob getLastQueueJob() {
            return new REQueueJob(Long.valueOf(1),123,null,null,2,"abc","def","hfg","ijk","egg");
        }

//        private REQueueModel() {
//            queueDao = DbHelper.getInstance().getDaoSession().getqueueDao();
//        }
//
//        public REQueueJob getLastQueueJob() {
//            return queueDao.queryBuilder().where(QueueDao.Properties.ServerId.isNotNull())
//                    .orderDesc(QueueDao.Properties.CreatedAt)
//                    .limit(1).unique();
//        }
//
//        public void insertOrReplace(REQueueJob queue) {
//            queueDao.insertOrReplace(queue);
//        }
//
//        public void insertOrReplaceAll(Collection<REQueueJob> queueJob) {
//            queueDao.insertOrReplaceInTx(queueJob);
//        }
//
//        public List<REQueueJob> loadQueues() {
//            return queueDao.queryBuilder().orderDesc(QueueDao.Properties.IsLocal, QueueDao.Properties.CreatedAt).listLazy();
//
//        }
//
//        public REQueueJob getQueueJobByLocalId(long localId) {
//            return queueDao.queryBuilder().where(QueueDao.Properties.LocalId.eq(localId)).limit(1).unique();
//        }
//
//        public void deleteQueueJobById(long localId) {
//            queueDao.deleteByKey(localId);
//        }
    }
