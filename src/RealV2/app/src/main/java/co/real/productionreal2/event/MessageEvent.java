package co.real.productionreal2.event;

/**
 * Created by kelvinsun on 27/5/16.
 */
public class MessageEvent {
    public final String message;

    public MessageEvent(String message) {
        this.message = message;
    }
}
