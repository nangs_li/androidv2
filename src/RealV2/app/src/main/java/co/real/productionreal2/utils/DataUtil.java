package co.real.productionreal2.utils;

import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import co.real.productionreal2.model.REAgentListing;
import co.real.productionreal2.model.REAgentProfile;
import co.real.productionreal2.model.REDBGoogleAddress;
import co.real.productionreal2.model.db.REAPIQueue;
import co.real.productionreal2.model.db.REDBAPIQueue;
import co.real.productionreal2.model.response.BaseResponse;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by kelvinsun on 13/7/16.
 */
public class DataUtil {

    public static List<REAPIQueue> getAllQueue() {
        Realm realm = Realm.getDefaultInstance();
        List<REAPIQueue> queueList = new ArrayList<>();
        RealmResults<REDBAPIQueue> results =
                realm.where(REDBAPIQueue.class)
                        .findAll();
        if (results.size() > 0) {
            for (REDBAPIQueue dbQueue : results) {
                REAPIQueue queue = new REAPIQueue();
                queue.queueId = dbQueue.queueId;
                queue.response = new Gson().fromJson(dbQueue.response, BaseResponse.class);
                queue.url = dbQueue.url;
                queue.parameter = dbQueue.parameter;
                queue.retryCount = dbQueue.retryCount;
                queueList.add(queue);
            }
        }
        return queueList;
    }

    public static REDBAPIQueue getDBQueueById(long queueId) {
        Realm realm = Realm.getDefaultInstance();
        REDBAPIQueue result =
                realm.where(REDBAPIQueue.class)
                        .equalTo("queueId", queueId)
                        .findFirst();
        return result;
    }

    public static REAPIQueue getQueueById(long queueId) {
        Realm realm = Realm.getDefaultInstance();
        REAPIQueue queue = new REAPIQueue();
        REDBAPIQueue result =
                realm.where(REDBAPIQueue.class)
                        .equalTo("queueId", queueId)
                        .findFirst();
        if (result != null) {
            queue.queueId = result.queueId;
            queue.response = new Gson().fromJson(result.response, BaseResponse.class);
            queue.url = result.url;
            queue.parameter = result.parameter;
            queue.retryCount = result.retryCount;
        }
        return queue;
    }


    public static List<REDBGoogleAddress> getLatestAddressSearchHistory() {
        Realm realm = Realm.getDefaultInstance();
        List<REDBGoogleAddress> addressList = new ArrayList<>();
        RealmResults<REDBGoogleAddress> results =
                realm.where(REDBGoogleAddress.class)
                        .findAllSorted("createdTime",Sort.ASCENDING);
        if (results.size() > 0) {
            final int MAX=5;
            int i=0;
            for (REDBGoogleAddress dbAddress : results) {
                //limit to 5 results
                if (i<MAX) {
                    Log.d("getLatest", "getHistory: " + dbAddress.toString());
                    addressList.add(dbAddress);
                    i++;
                }else{
                    break;
                }
            }
        }
        return addressList;
    }

    public static REAgentProfile getAgentProfileByMId(int mId) {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(REAgentProfile.class)
                .findFirst();
    }

    public static REAgentListing getAgentListingByLId(int lId) {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(REAgentListing.class)
                .findFirst();
    }

    public static List<REAgentProfile> getAgentProfileList() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<REAgentProfile> results =
                realm.where(REAgentProfile.class)
                        .findAll();
        return results;
    }

}
