package co.real.productionreal2.model;

import android.os.Parcel;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by kelvinsun on 20/5/16.
 */
public class REAgentProfile extends RealmObject {
    @PrimaryKey
    @SerializedName("MemberID")
    public int realMemberID;
    @SerializedName("MemberType")
    public int memberType;
    @SerializedName("MemberName")
    public String memberName;
    @SerializedName("PhotoURL")
    public String photoURL;
    @SerializedName("LicenseNumber")
    public String licenseNumber;
    @SerializedName("QBID")
    public String qBID;
    @SerializedName("FollowingCount")
    public int followingCount;
    @SerializedName("FollowerCount")
    public int followerCount;
    @SerializedName("IsFollowing")
    public int isFollowing;
    @SerializedName("Version")
    public int version;
    @Nullable
    @SerializedName("Experiences")
    public RealmList<REExperience> experiencesList;
    @Nullable
    @SerializedName("Languages")
    public RealmList<RELanguage> languagesList;
//    @Nullable
//    @SerializedName("Specialties")
//    public RealmList<Specialties> specialtiesList;
//    @Nullable
//    @SerializedName("PastClosings")
//    public RealmList<PastClosings> pastClosingsList;

    public REAgentProfile(Parcel source) {
        realMemberID = source.readInt();
        licenseNumber = source.readString();
        isFollowing = source.readInt();
        experiencesList =source.readParcelable(REExperience.class.getClassLoader());
        languagesList =source.readParcelable(RELanguage.class.getClassLoader());
//        specialtiesList =source.readParcelable(Specialties.class.getClassLoader());
//        pastClosingsList =source.readParcelable(PastClosings.class.getClassLoader());
    }

    public REAgentProfile() {
    }

    @Override
    public String toString() {
        return "REAgentProfile{" +
                "realMemberID=" + realMemberID +
                ", memberType=" + memberType +
                ", memberName='" + memberName + '\'' +
                ", photoURL='" + photoURL + '\'' +
                ", licenseNumber='" + licenseNumber + '\'' +
                ", qBID='" + qBID + '\'' +
                ", followingCount=" + followingCount +
                ", followerCount=" + followerCount +
                ", isFollowing=" + isFollowing +
                ", version=" + version +
                ", experiencesList=" + experiencesList +
                ", languagesList=" + languagesList +
//                ", specialtiesList=" + specialtiesList +
//                ", PastClosingsList=" + pastClosingsList +
                '}';
    }


//    private class Specialties extends RealmObject{
//        @SerializedName("Language")
//        public String language;
//    }
//
//    private class PastClosings extends RealmObject{
//        @SerializedName("Language")
//        public String language;
//    }


}
