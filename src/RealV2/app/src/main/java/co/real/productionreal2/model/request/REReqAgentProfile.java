package co.real.productionreal2.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import co.real.productionreal2.REApplication;
import co.real.productionreal2.model.response.REBaseResponse;
import co.real.productionreal2.network.REApiCallback;
import co.real.productionreal2.network.REApiClient;
import retrofit2.Call;

/**
 * Created by kelvinsun on 15/7/16.
 */
public class REReqAgentProfile extends REBaseRequest {
    @SerializedName("AccessToken")
    public String accessToken;

    @SerializedName("MemberID")
    public int memberID;

    @SerializedName("RetrieveList")
    public List<Integer> retrieveList;

    public REReqAgentProfile(int languageIndex, String uniqueKey, String accessToken, int memberID, List<Integer> retrieveList) {
        super("AgentProfileGet", languageIndex, uniqueKey);
        this.accessToken = accessToken;
        this.memberID = memberID;
        this.retrieveList = retrieveList;
    }


    public void callAgentProfileGet(REApiClient reApiClient, final REApiCallback callback) {

        Call<REBaseResponse> makeCall = REApplication.getReApiService().agentProfileGet(this);
        makeCall.enqueue(reApiClient.getJSONResponseCallBack(callback));
    }
}
