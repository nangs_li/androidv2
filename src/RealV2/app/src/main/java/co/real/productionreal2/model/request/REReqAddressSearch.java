package co.real.productionreal2.model.request;

import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.REApplication;
import co.real.productionreal2.model.response.REBaseResponse;
import co.real.productionreal2.network.REApiCallback;
import co.real.productionreal2.network.REApiClient;
import retrofit2.Call;

/**
 * Created by kelvinsun on 15/7/16.
 */
public class REReqAddressSearch extends REBaseRequest {

    @SerializedName("AccessToken")
    public String accessToken;

    @SerializedName("MemberID")
    public int memberID;

    @SerializedName("PlaceID")
    public String placeID;

    public REReqAddressSearch(int languageIndex, String uniqueKey,String accessToken, int memberID, String placeID) {
        super("AddressSearch", languageIndex, uniqueKey);
        this.accessToken = accessToken;
        this.memberID = memberID;
        this.placeID = placeID;
    }

    public void callAddressSearch(REApiClient reApiClient, final REApiCallback callback) {

        Call<REBaseResponse> makeCall = REApplication.getReApiService().addressSearch(this);
        makeCall.enqueue(reApiClient.getJSONResponseCallBack(callback));
    }


}
