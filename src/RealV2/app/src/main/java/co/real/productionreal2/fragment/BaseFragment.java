package co.real.productionreal2.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.real.productionreal2.R;
import io.realm.Realm;

public class BaseFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public REMainFragment getMainFragment(){
        return (REMainFragment)getFragmentManager().findFragmentByTag("MAIN_FRAGMENT");
    }
}
