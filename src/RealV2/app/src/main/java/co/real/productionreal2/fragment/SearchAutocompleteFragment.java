package co.real.productionreal2.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import java.util.ArrayList;
import java.util.List;

import co.real.productionreal2.R;
import co.real.productionreal2.adapter.SearchAutocompleteAdapter;
import co.real.productionreal2.model.REDBGoogleAddress;
import co.real.productionreal2.utils.AppUtil;
import customui.SimpleDividerItemDecoration;

/**
 * Created by kelvinsun on 14/7/16.
 */
public class SearchAutocompleteFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    RecyclerView rvAddressList;
    SearchAutocompleteAdapter searchAutocompleteAdapter;

    SearchAutocompleteAdapterListener mCallback;

    // Container Activity must implement this interface
    public interface SearchAutocompleteAdapterListener {
        public void onSearchResult(List<REDBGoogleAddress> autocompleteAddressList);

        public void onAddressSelected(REDBGoogleAddress address);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_autocomplete, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvAddressList = (RecyclerView) view.findViewById(R.id.rv_address);

        initView();

        addListener();
    }

    private void addListener() {

    }

    private void initView() {
        rvAddressList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rvAddressList.setLayoutManager(llm);
        rvAddressList.setItemAnimator(new DefaultItemAnimator());
        rvAddressList.addItemDecoration(new SimpleDividerItemDecoration(getResources()));

    }

    public void updateView(List<REDBGoogleAddress> autocompleteAddressList) {
        searchAutocompleteAdapter = new SearchAutocompleteAdapter(autocompleteAddressList, this);
        rvAddressList.setAdapter(searchAutocompleteAdapter);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        REDBGoogleAddress address = searchAutocompleteAdapter.getItem(position);
        AppUtil.showToast(getActivity(), address.placeId);
        Log.d("getPlaceId", "getPlaceId: " + address.placeId);
        //clear search results
        updateView(new ArrayList<REDBGoogleAddress>());

        //TODO: call address search api
        //update searchview
        mCallback.onAddressSelected(address);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (SearchAutocompleteAdapterListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
}
