package co.real.productionreal2.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.real.productionreal2.R;
import co.real.productionreal2.REApplication;
import co.real.productionreal2.fragment.REMainFragment;
import customui.RETabBar;
import customui.RETabView;

/**
 * Created by kelvinsun on 4/8/16.
 */
public class REMainActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener{

    @BindView(R.id.fl_main_container)
    FrameLayout mainContainer;

    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initView();

        REApplication.get().mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();
    }

    private void initView() {
        android.support.v4.app.FragmentTransaction ft= getSupportFragmentManager().beginTransaction();
        ft.add(R.id.fl_main_container, new REMainFragment(),"MAIN_FRAGMENT");
        ft.addToBackStack("MAIN_FRAGMENT");
        ft.commit();
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
