package co.real.productionreal2.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kelvinsun on 20/5/16.
 */
public class REAgentLanguage {

    @SerializedName("Language")
    public String lang;

    @SerializedName("LangIndex")
    public int langIndex;

    public long id;

    public REAgentLanguage() {
    }

    public REAgentLanguage(int langIndex, String lang) {
        this.langIndex = langIndex;
        this.lang = lang;
    }
}
