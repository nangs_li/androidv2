package co.real.productionreal2.model;

import android.os.Parcel;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by kelvinsun on 15/7/16.
 */
public class REAgentListing  extends RealmObject {

    @PrimaryKey
    @SerializedName("AgentListingID")
    public int agentListingID;
    @SerializedName("Version")
    public int version;
    @SerializedName("CreationDate")
    public String creationDate;
    @SerializedName("SloganIndex")
    public int sloganIndex;
    @SerializedName("PropertyType")
    public int propertyType;
    @SerializedName("SpaceType")
    public int spaceType;
    @SerializedName("PropertyPrice")
    public long propertyPrice;
    @SerializedName("PropertySize")
    public long propertySize;
    @SerializedName("CurrencyUnit")
    public String currencyUnit;
    @SerializedName("SizeUnit")
    public String sizeUnit;
    @SerializedName("SizeUnitType")
    public int sizeUnitType;
    @SerializedName("BedroomCount")
    public int bedroomCount;
    @Nullable
    @SerializedName("Address")
    public RealmList<REAddress> addressList;
    @Nullable
    @SerializedName("Reasons")
    public RealmList<REReason> reasonsList;
    @Nullable
    @SerializedName("Photos")
    public RealmList<REPhoto> photosList;

    public REAgentListing(Parcel source) {
        creationDate = source.readString();
        agentListingID = source.readInt();
        sloganIndex=source.readInt();
        propertyType=source.readInt();
        spaceType=source.readInt();
        propertyPrice=source.readLong();
        propertySize=source.readLong();
        currencyUnit=source.readString();
        sizeUnit=source.readString();
        sizeUnitType=source.readInt();
        bedroomCount=source.readInt();
        addressList =source.readParcelable(REAddress.class.getClassLoader());
//        reasonsList =source.readParcelable(Reason.class.getClassLoader());
//        photosList =source.readParcelable(Photo.class.getClassLoader());
    }

    public REAgentListing() {
    }

    public REAddress getAddressWithLocale(){
        if (addressList==null)
            return null;

        if (addressList.size()>0) {
            return addressList.get(0);
        }else{
            return null;
        }
    }

    public String getFullAddressWithLocale(){
        if (addressList==null)
            return null;

        if (addressList.size()>0) {
            return addressList.get(0).formattedAddress;
        }else{
            return null;
        }
    }

    public String getListingCoverPhoto(){
        if (photosList==null)
            return null;

        if (photosList.size()>0) {
            return photosList.get(0).URL;
        }else{
            return null;
        }
    }

    @Override
    public String toString() {
        return "REAgentListing{" +
                "creationDate='" + creationDate + '\'' +
                ", agentListingID=" + agentListingID +
                ", sloganIndex=" + sloganIndex +
                ", propertyType=" + propertyType +
                ", spaceType=" + spaceType +
                ", propertyPrice=" + propertyPrice +
                ", propertySize=" + propertySize +
                ", currencyUnit='" + currencyUnit + '\'' +
                ", sizeUnit='" + sizeUnit + '\'' +
                ", sizeUnitType=" + sizeUnitType +
                ", bedroomCount=" + bedroomCount +
                ", addressList='" + addressList + '\'' +
                ", reasonsList=" + reasonsList +
                ", photosList=" + photosList +
                '}';
    }

}
