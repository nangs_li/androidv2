package co.real.productionreal2.model.request;

import com.google.gson.Gson;

/**
 * Created by kelvinsun on 20/5/16.
 */
public class BaseRequest {
    final String InputJson;

    public BaseRequest(Object reqObject) {
        this.InputJson = new Gson().toJson(reqObject);
    }

}
