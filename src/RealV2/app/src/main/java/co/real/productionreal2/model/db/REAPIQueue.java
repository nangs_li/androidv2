package co.real.productionreal2.model.db;

import java.util.List;

import co.real.productionreal2.REConstants;
import co.real.productionreal2.model.response.BaseResponse;

/**
 * Created by edwinchan on 8/7/16.
 */
public class REAPIQueue {

    public long queueId;
    public int httpMethod;
    public List<Integer> dependingQueues;
    public List<Integer> pendingQueues;
    public int retryCount;
    public BaseResponse response;
    public String url;
    public String parameter;
    public REConstants.API_QUEUE_STATUS status;

    public REAPIQueue() {
    }

    public REAPIQueue(long queueId, int httpMethod, List<Integer> dependingQueues, List<Integer> pendingQueues, int retryCount, BaseResponse response, String url, String parameter, REConstants.API_QUEUE_STATUS status) {
        this.queueId = queueId;
        this.httpMethod = httpMethod;
        this.dependingQueues = dependingQueues;
        this.pendingQueues = pendingQueues;
        this.retryCount = retryCount;
        this.response = response;
        this.url = url;
        this.parameter = parameter;
        this.status = status;
    }

    @Override
    public String toString() {
        return "REAPIQueue{" +
                "queueId=" + queueId +
                ", httpMethod=" + httpMethod +
                ", dependingQueues=" + dependingQueues +
                ", pendingQueues=" + pendingQueues +
                ", retryCount=" + retryCount +
                ", response=" + response +
                ", url='" + url + '\'' +
                ", parameter='" + parameter + '\'' +
                ", status=" + status +
                '}';
    }
}