package co.real.productionreal2.activity;

import android.os.Bundle;
import android.os.SystemClock;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.real.productionreal2.R;
import co.real.productionreal2.model.db.TestingDBUser;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class TestingDBActivity extends BaseActivity {

    @BindView(R.id.tv_testingdb_result)
    TextView tv_TestingDBResult;

    private Random random = new Random();
    private Thread backgroundThread;

    int uerId = 0;
    int bguerId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testingdb);

        ButterKnife.bind(this);
//
//        initData();
//        initView();
//        addListener();

    }
    @Override
    public void onResume() {
        super.onResume();
        // Enable UI refresh while the fragment is active.
        realm.addChangeListener(realmListener);

        // Create background thread that add a new dot every 0.5 second.
        backgroundThread = new Thread() {

            @Override
            public void run() {
                // Realm instances cannot be shared between threads, so we need to create a new
                // instance on the background thread.
                final Realm backgroundThreadRealm = Realm.getDefaultInstance();
                while (!backgroundThread.isInterrupted()) {
                    backgroundThreadRealm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
//                            try {
                                backgroundThreadRealm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        TestingDBUser testingDBUser = realm.createObject(TestingDBUser.class);
                                        testingDBUser.setName("bgu_" + bguerId++);
                                    }
                                });
//                            }catch (Exception e) {
//                                Log.e("Realm Error", "error "+ e);
//                                backgroundThreadRealm.cancelTransaction();
//                            }
                        }
                    });

                    // Wait 0.5 sec. before adding the next dot.
                    SystemClock.sleep(500);
                }

                // Also close Realm instances used in background threads.
                backgroundThreadRealm.close();
            }
        };
        backgroundThread.start();

    }

    @Override
    public void onPause() {
        super.onPause();

        // Disable UI refresh while the fragment is no longer active.
        realm.removeChangeListener(realmListener);
        backgroundThread.interrupt();
    }


    private void initData() {
    }

    private void initView() {

    }

    private void addListener() {

    }

    // Realm change listener that refreshes the UI when there is changes to Realm.
    private RealmChangeListener<Realm> realmListener = new RealmChangeListener<Realm>() {
        @Override
        public void onChange(Realm realm) {
            String Datetime;
            Calendar c = Calendar.getInstance();
            SimpleDateFormat dateformat = new SimpleDateFormat("hh:mm:ss.SSS aa");
            Datetime = dateformat.format(c.getTime());
            tv_TestingDBResult.append("onChange start: "+ Datetime+"\n");

            RealmResults<TestingDBUser> results = realm.where(TestingDBUser.class).findAll();
            String text = "";
            for(TestingDBUser testingDBUser : results){
                text += testingDBUser.getName() + " ";
                tv_TestingDBResult.append(testingDBUser.getName() + " ");
            }
//            tv_TestingDBResult.setText(text);

            c = Calendar.getInstance();
            Datetime = dateformat.format(c.getTime());
            tv_TestingDBResult.append("onChange end: "+ Datetime+"\n");
        }
    };




    @OnClick(R.id.bn_testingdb_start)
    void clickTestingDBStart() {
        String Datetime;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateformat = new SimpleDateFormat("hh:mm:ss.SSS aa");
        Datetime = dateformat.format(c.getTime());
        tv_TestingDBResult.append("Start: "+ Datetime+"\n");
        int i = 0;
        while(i<100) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    TestingDBUser testingDBUser = realm.createObject(TestingDBUser.class);
                    testingDBUser.setName("u_" + uerId++);
                }
            });
            i++;
        }
        c = Calendar.getInstance();
        Datetime = dateformat.format(c.getTime());
        tv_TestingDBResult.append("End: "+ Datetime+"\n");
    }

    @OnClick(R.id.bn_testingdb_clear)
    void clickTestingDBClear() {
        tv_TestingDBResult.setText("");
        uerId = 0;
        bguerId = 0;
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(TestingDBUser.class);
            }
        });
    }



}
