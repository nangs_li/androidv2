package co.real.productionreal2.utils;

import android.app.Activity;
import android.content.Intent;

import co.real.productionreal2.R;

/**
 * Created by edwinchan on 20/5/16.
 */
public class NavigationUtil {

    public final static String EXTRA_NAVIGATION_TYPE_KEY = "EXTRA_NAVIGATION_TYPE_KEY";

    /**
     * Navigation Animation Type
     */
    public static enum NavigationType {
        SlideUp, SlideIn, None, Fade
    }

    /**
     * Get NavigationType From Intent
     *
     * @param intent
     */
    public static NavigationType getNavigationTypeFromIntent(Intent intent) {
        if (intent.hasExtra(EXTRA_NAVIGATION_TYPE_KEY)) {
            int typeIndex = intent.getIntExtra(EXTRA_NAVIGATION_TYPE_KEY, 0);
            return NavigationType.values()[typeIndex];
        } else {
            return NavigationType.SlideUp;
        }
    }

    /**
     * Strat Activity with default Animation
     *
     * @param fromActivity
     * @param intent
     */
    public static void startActivity(Activity fromActivity, Intent intent) {
        startActivity(fromActivity, intent, NavigationType.SlideUp);
    }

    /**
     * Strat Activity with custom Animation
     *
     * @param fromActivity
     * @param intent
     * @param navigationType
     */
    public static void startActivity(Activity fromActivity, Intent intent, NavigationType navigationType) {
        int intNavigationType = getOrdinal(navigationType);
        intent.putExtra(EXTRA_NAVIGATION_TYPE_KEY, intNavigationType);
        fromActivity.startActivity(intent);
        setStratActivityAnimation(fromActivity, intNavigationType);
    }

    /**
     * Strat Activity For Result with default Animation
     *
     * @param fromActivity
     * @param intent
     * @param requestCode
     */
    public static void startActivityForResult(Activity fromActivity, Intent intent, int requestCode) {
        startActivityForResult(fromActivity, intent, requestCode, NavigationType.SlideUp);
    }

    /**
     * Strat Activity For Result with custom Animation
     *
     * @param fromActivity
     * @param intent
     * @param requestCode
     * @param navigationType
     */
    public static void startActivityForResult(Activity fromActivity, Intent intent, int requestCode, NavigationType navigationType) {
        int intNavigationType = getOrdinal(navigationType);
        intent.putExtra(EXTRA_NAVIGATION_TYPE_KEY, intNavigationType);
        fromActivity.startActivityForResult(intent, requestCode);
        setStratActivityAnimation(fromActivity, intNavigationType);
    }

    /**
     * Returns the position of the enum constant in the declaration.
     *
     * @param navigationType
     * @return the ordinal value of this enum constant.
     */
    private static int getOrdinal(NavigationType navigationType) {
        return navigationType.ordinal();
    }

    /**
     * Setup Activity Animation
     *
     * @param fromActivity
     * @param intNavigationType
     */
    private static void setStratActivityAnimation(Activity fromActivity, int intNavigationType) {
        switch (intNavigationType) {
            case 1: //SlideIn
                fromActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
            case 2: //None
                fromActivity.overridePendingTransition(0, 0);
                break;
            case 3: //Fade
                fromActivity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case 0: //SlideUp
            default:
                fromActivity.overridePendingTransition(R.anim.slide_in_up, R.anim.fade_out);
                break;
        }
    }


}
