package co.real.productionreal2.utils;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import co.real.productionreal2.BuildConfig;
import co.real.productionreal2.REConstants;
import co.real.productionreal2.R;

/**
 * Created by kelvinsun on 19/5/16.
 */
public class AppUtil {

    private static Toast mToast = null;

    /**
     * General message show in toast
     *
     * @param context
     * @param message
     */
    public static void showToast(Context context, String message) {
        if (context == null) return;

        if (!TextUtils.isEmpty(message) && message.trim() != "") {
            if (mToast != null) {
                mToast.cancel();
            }
            mToast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
            mToast.show();
        }
    }

    /**
     * General system message show in toast
     *
     * @param context
     * @param messageId
     */
    public static void showToast(Context context, int messageId) {

        if (context == null) return;

        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(context, messageId, Toast.LENGTH_SHORT);
        mToast.show();
    }

    /**
     * get the app version
     *
     * @param context
     * @return
     */
    public static String getAppVersion(Context context) {
        String versionName = "";
        if (REConstants.isDevelopment) {
            versionName = BuildConfig.VERSION_NAME + " (" + BuildConfig.FLAVOR + ")";
        } else {
            versionName = BuildConfig.VERSION_NAME;
        }
        return context.getString(R.string.about_this_version__version) +" : "+ versionName;
    }


    public static String getBuildVersion(Context context) {
        return context.getString(R.string.about_this_version__build) + " : "+ BuildConfig.BuildNumber + " [" + BuildConfig.GitHash + "]";
    }
}
