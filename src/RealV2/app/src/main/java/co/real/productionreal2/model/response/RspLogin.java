package co.real.productionreal2.model.response;

import com.google.gson.annotations.SerializedName;

import co.real.productionreal2.model.REAgentProfile;

/**
 * Created by kelvinsun on 26/5/16.
 */
public class RspLogin extends BaseResponse {

    @SerializedName("content")
    public RspContent content;

    public RspLogin(RspContent content) {
        super();
        this.content = content;
    }

    @Override
    public String toString() {
        return "RspLogin{" +
                "content=" + content +
                '}';
    }

    public class RspContent {

        @SerializedName("memberID")
        public String memberID;

        @SerializedName("memberName")
        public String memberName;

        @SerializedName("accessToken")
        public String accessToken;

        @SerializedName("qbID")
        public String qbID;

        @SerializedName("qbPwd")
        public String qbPwd;

        @SerializedName("photoURL")
        public String photoURL;

        @SerializedName("memberType")
        public int memberType;

        @SerializedName("followerCount")
        public int followerCount;

        @SerializedName("followingCount")
        public int followingCount;

        @SerializedName("loginAs")
        public int loginAs;

        @SerializedName("REAgentProfile")
        public REAgentProfile REAgentProfile;

        public RspContent(String memberID, String memberName, String accessToken, String qbID, String qbPwd, String photoURL, int memberType, int followerCount, int followingCount, int loginAs, REAgentProfile REAgentProfile) {
            super();
            this.memberID = memberID;
            this.memberName = memberName;
            this.accessToken = accessToken;
            this.qbID = qbID;
            this.qbPwd = qbPwd;
            this.photoURL = photoURL;
            this.memberType = memberType;
            this.followerCount = followerCount;
            this.followingCount = followingCount;
            this.loginAs = loginAs;
            this.REAgentProfile = REAgentProfile;
        }

        @Override
        public String toString() {
            return "RspLogin{" +
                    "memberID='" + memberID + '\'' +
                    ", memberName='" + memberName + '\'' +
                    ", accessToken='" + accessToken + '\'' +
                    ", qbID='" + qbID + '\'' +
                    ", qbPwd='" + qbPwd + '\'' +
                    ", photoURL='" + photoURL + '\'' +
                    ", memberType=" + memberType +
                    ", followerCount=" + followerCount +
                    ", followingCount=" + followingCount +
                    ", loginAs=" + loginAs +
                    ", REAgentProfile=" + REAgentProfile +
                    '}';
        }
    }
}
