package co.real.productionreal2.activity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.path.android.jobqueue.AsyncAddCallback;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.real.productionreal2.R;
import co.real.productionreal2.REApplication;
import co.real.productionreal2.REConstants;
import co.real.productionreal2.jobs.CallApiJob;
import co.real.productionreal2.model.db.REAPIQueue;
import co.real.productionreal2.model.db.REDBAPIQueue;
import co.real.productionreal2.model.db.TestingDBUser;
import co.real.productionreal2.utils.DataUtil;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by kelvinsun on 13/7/16.
 */
public class RemoteDemoActivity extends BaseActivity {

    @BindView(R.id.tv_testingdb_result)
    TextView tv_TestingDBResult;

    int uerId = 0;
    int bguerId = 0;
    private int testPriority=1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testingdb);

        ButterKnife.bind(this);

        realm.beginTransaction();
        realm.delete(REDBAPIQueue.class);
        realm.commitTransaction();
//
//        initData();
//        initView();
//        addListener();

    }
    @Override
    public void onResume() {
        super.onResume();
        // Enable UI refresh while the fragment is active.

        realm.addChangeListener(realmListener);

    }

    @Override
    public void onPause() {
        super.onPause();
        // Disable UI refresh while the fragment is no longer active.
        realm.removeChangeListener(realmListener);
    }


    private void initData() {
    }

    private void initView() {

    }

    private void addListener() {

    }

    // Realm change listener that refreshes the UI when there is changes to Realm.
    private RealmChangeListener<Realm> realmListener = new RealmChangeListener<Realm>() {
        @Override
        public void onChange(Realm realm) {
            String Datetime;
            Calendar c = Calendar.getInstance();
            SimpleDateFormat dateformat = new SimpleDateFormat("hh:mm:ss.SSS aa");
            Datetime = dateformat.format(c.getTime());
            tv_TestingDBResult.append("onChange start: "+ Datetime+"\n");

            RealmResults<TestingDBUser> results = realm.where(TestingDBUser.class).findAll();
            String text = "";
            for(TestingDBUser testingDBUser : results){
                text += testingDBUser.getName() + " ";
                tv_TestingDBResult.append(testingDBUser.getName() + " ");
            }
//            tv_TestingDBResult.setText(text);

            c = Calendar.getInstance();
            Datetime = dateformat.format(c.getTime());
            tv_TestingDBResult.append("onChange end: "+ Datetime+"\n");
        }
    };




    @OnClick(R.id.bn_testingdb_start)
    void clickTestingDBStart() {
        String Datetime;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateformat = new SimpleDateFormat("hh:mm:ss.SSS aa");
        Datetime = dateformat.format(c.getTime());
        tv_TestingDBResult.append("Start: "+ Datetime+"\n");
        int i = 0;
        while(i<100) {
            realm.beginTransaction();
            TestingDBUser testingDBUser = realm.createObject(TestingDBUser.class);
            testingDBUser.setName("bgu_" + bguerId++);
            realm.commitTransaction();
            i++;
        }
        c = Calendar.getInstance();
        Datetime = dateformat.format(c.getTime());
        tv_TestingDBResult.append("End: "+ Datetime+"\n");
    }

    @OnClick(R.id.bn_testingdb_clear)
    void clickTestingDBClear() {
        tv_TestingDBResult.setText("");
        uerId = 0;
        bguerId = 0;
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(TestingDBUser.class);
            }
        });
    }

    @OnClick(R.id.bn_add_job)
    void clickAddJob(){
        REAPIQueue reapiQueue=new REAPIQueue();
        reapiQueue.url="test";
        reapiQueue.retryCount=3;
        reapiQueue.parameter="abc";
        reapiQueue.status= REConstants.API_QUEUE_STATUS.PENDING;
        testPriority=testPriority-100;
        REApplication.get().getJobManager().addJobInBackground(new CallApiJob(reapiQueue, Integer.valueOf(testPriority)), new AsyncAddCallback() {
            @Override
            public void onAdded(long jobId) {

            }
        });
    }

    @OnClick(R.id.bn_show_existing_job)
    void clickShowJob(){
        Log.d("showJobs","showJobs: "+DataUtil.getAllQueue().size());
        for (REAPIQueue queue:DataUtil.getAllQueue()){
            tv_TestingDBResult.append(queue.queueId+ " status: "+ " " +queue.parameter + " \n");
        }
    }



}