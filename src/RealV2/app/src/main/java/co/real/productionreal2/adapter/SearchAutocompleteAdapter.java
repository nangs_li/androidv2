package co.real.productionreal2.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.real.productionreal2.R;
import co.real.productionreal2.model.REDBGoogleAddress;

/**
 * Created by kelvinsun on 14/7/16.
 */
public class SearchAutocompleteAdapter extends RecyclerView.Adapter<SearchAutocompleteAdapter.AddressViewHolder>{

    List<REDBGoogleAddress> autocompleteAddressList=new ArrayList<>();
    AdapterView.OnItemClickListener onItemClickListener;

    public SearchAutocompleteAdapter(List<REDBGoogleAddress> autocompleteAddressList,AdapterView.OnItemClickListener onItemClickListener) {
        this.autocompleteAddressList = autocompleteAddressList;
        this.onItemClickListener = onItemClickListener;
    }

    public SearchAutocompleteAdapter(List<REDBGoogleAddress> autocompleteAddressList) {
        this.autocompleteAddressList = autocompleteAddressList;
    }

    @Override
    public AddressViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_address,parent,false);
        return new AddressViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AddressViewHolder holder, int position) {
        Log.d("onBindViewHolder","onBindViewHolder: "+position);
        REDBGoogleAddress address=autocompleteAddressList.get(position);
        holder.addressName.setText(address.name);
//        Log.d("onBindViewHolder", "onBindViewHolder: 1st: "+address.getPrimaryText(null));
//        Log.d("onBindViewHolder", "onBindViewHolder: 2nd: "+address.getSecondaryText(null));

    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public REDBGoogleAddress getItem(int position) {
        return autocompleteAddressList.get(position);
    }


    @Override
    public int getItemCount() {
        return autocompleteAddressList.size();
    }

    public class AddressViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{
        protected TextView addressName;

        public AddressViewHolder(View v) {
            super(v);
            addressName =  (TextView) v.findViewById(R.id.tv_listing_name);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(null, v, getAdapterPosition(), v.getId());
        }
    }
}
